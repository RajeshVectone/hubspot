﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace TestHUBSpot
{
    class Program
    {
        static void Main(string[] args)
        {

            WriteStartEnd("Request Start.." + DateTime.Now.ToString());
            //Thread worker = new Thread(ExecuteMethod);
            //worker.Start();

            // for (int i = 0; i < 2; i++)
            // {
            Thread.Sleep(3000);
            ExecuteMethod();
            WriteStartEnd("Request End.." + DateTime.Now.ToString());
            //}
            // ExecuteMethod();
        }
        private static bool checkEmailHubspot(string contactID)
        {
            //123405951
            //123224201
            string URL = ConfigurationManager.AppSettings["searchcontact"].ToString();
            URL = string.Format(URL, contactID);
            HttpWebRequest httpupdateReq = (HttpWebRequest)WebRequest.Create(URL);
            httpupdateReq.ProtocolVersion = HttpVersion.Version11;
            httpupdateReq.Method = "GET";
            try
            {
                HttpWebResponse Updateresponse = (HttpWebResponse)httpupdateReq.GetResponse();
                string s = Updateresponse.ToString();
                StreamReader Updatereader = new StreamReader(Updateresponse.GetResponseStream());
                String jsonresponse = "";
                String temp = null;
                while ((temp = Updatereader.ReadLine()) != null)
                {
                    jsonresponse += temp;
                }
                //Assigning Unique values for update 
                checkEmailHubspotDetails dataResult = JsonConvert.DeserializeObject<checkEmailHubspotDetails>(jsonresponse);
                string updatevid = string.Empty;
                string isNew = string.Empty;
                if (dataResult != null)
                {
                    try
                    {
                        updatevid = dataResult.properties.email.ToString();

                    }
                    catch (Exception ex)
                    {
                        return false;
                    }

                }
                if (string.IsNullOrEmpty(updatevid))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public class checkEmailHubspotProperties
        {
            public DateTime createdate { get; set; }
            public object email { get; set; }
            public string firstname { get; set; }
            public string hs_object_id { get; set; }
            public DateTime lastmodifieddate { get; set; }
            public string lastname { get; set; }
        }

        public class checkEmailHubspotDetails
        {
            public string id { get; set; }
            public checkEmailHubspotProperties properties { get; set; }
            public DateTime createdAt { get; set; }
            public DateTime updatedAt { get; set; }
            public bool archived { get; set; }
        }
        private static void ExecuteMethod()
        {

            try
            {

                string scenario = ConfigurationManager.AppSettings["Scenario"].ToString();
                if (scenario == "0")
                {
                    //ServicePointManager.DefaultConnectionLimit = 200;
                    //ServicePointManager.DefaultConnectionLimit = 500;

                    //Change max connections from .NET to a remote service default: 2
                    System.Net.ServicePointManager.DefaultConnectionLimit = 4000;
                    //Bump up the min threads reserved for this app to ramp connections faster - minWorkerThreads defaults to 4, minIOCP defaults to 4 
                    System.Threading.ThreadPool.SetMinThreads(100, 100);
                    //Turn off the Expect 100 to continue message - 'true' will cause the caller to wait until it round-trip confirms a connection to the server 
                    System.Net.ServicePointManager.Expect100Continue = false;
                    //Can decreas overall transmission overhead but can cause delay in data packet arrival
                    System.Net.ServicePointManager.UseNagleAlgorithm = false;


                    UrcrmusergetdetailsInput objUrcrmusergetdetailsInput = new UrcrmusergetdetailsInput();
                    objUrcrmusergetdetailsInput.fromdate = DateTime.Now.ToString("yyyy-MM-dd");
                    objUrcrmusergetdetailsInput.todate = DateTime.Now.AddMonths(1).ToString("yyyy-MM-dd");

                    SyncCustomersInput objSyncCustomersInput = new SyncCustomersInput();
                    objSyncCustomersInput.processtype = 1;
                    List<UrcrmusergetdetailsOutput2> objResult = GetSyncCustomers1(objSyncCustomersInput);

                    //string objResultJSONData = JsonConvert.SerializeObject(objResult);
                    Properties objProperties = new Properties();
                    List<Contact> objList = new List<Contact>();

                    if (objResult.Count > 0 && objResult[0].errcode != -1)
                    {

                        WriteStartEnd("Count.." + objResult.Count);
                        try
                        {
                            int i = 0;
                            foreach (UrcrmusergetdetailsOutput2 objUrcrmusergetdetailsOutput in objResult)
                            {
                                // if (objUrcrmusergetdetailsOutput.hs_object_id == "437418")
                                // {


                                //Exception after 2 records
                                //if (i > 0)
                                //{
                                //    if (i % 2 == 0)
                                //        return;
                                //}


                                string Url = ConfigurationManager.AppSettings["LIVEURL"].ToString();
                                string UPDATELIVEURL = ConfigurationManager.AppSettings["UPDATELIVEURL"].ToString();

                                CreateProperties1(objUrcrmusergetdetailsOutput, out objList);
                                if (!string.IsNullOrEmpty(objUrcrmusergetdetailsOutput.hs_object_id))
                                    UPDATELIVEURL = UPDATELIVEURL.Replace("{0}", objUrcrmusergetdetailsOutput.hs_object_id.ToString());

                                //Assigning new value properties to objects
                                #region Assign new value
                                Encoding encoding = new UTF8Encoding();
                                objProperties.properties = objList;
                                string postData = JsonConvert.SerializeObject(objProperties);
                                byte[] data = encoding.GetBytes(postData);
                                HttpWebRequest httpWReq = null;
                                string visitorid = string.Empty;
                                //httpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(Url);

                                if (objUrcrmusergetdetailsOutput.hs_object_id != null && objUrcrmusergetdetailsOutput.hs_object_id.ToString() != null && objUrcrmusergetdetailsOutput.hs_object_id.ToString() != "0")
                                {
                                    //Check again for Contact ID.
                                    httpWReq = (HttpWebRequest)WebRequest.Create(UPDATELIVEURL);
                                }
                                else
                                {
                                    httpWReq = (HttpWebRequest)WebRequest.Create(Url);
                                }

                                try
                                {

                                    httpWReq.ProtocolVersion = HttpVersion.Version11;
                                    httpWReq.Method = "POST";
                                    httpWReq.ContentType = "application/json";

                                    Stream stream = httpWReq.GetRequestStream();
                                    stream.Write(data, 0, data.Length);
                                    stream.Close();

                                    #region GetNewValueResponse
                                    HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                                    string s = response.ToString();
                                    StreamReader reader = new StreamReader(response.GetResponseStream());
                                    String jsonresponse = "";
                                    String temp = null;

                                    var text = reader.ReadToEnd();

                                    while ((temp = reader.ReadLine()) != null)
                                    {
                                        jsonresponse += temp;
                                    }
                                    //Assigning Unique values for update
                                    //var dataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);
                                    var dataResult = (JObject)JsonConvert.DeserializeObject(text);
                                    string vid = string.Empty;
                                    string isNew = string.Empty;
                                    if (dataResult != null)
                                    {
                                        vid = dataResult["vid"].Value<string>();
                                        // isNew = dataResult["isNew"].Value<string>();
                                    }
                                    #endregion


                                    #region UpdateContactID to DB

                                    SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                    objSyncUpdateContactIDInput.processtype = 2;//For Updating in our DB

                                    if (objUrcrmusergetdetailsOutput.hs_object_id != null && objUrcrmusergetdetailsOutput.hs_object_id.ToString() != null)
                                    {
                                        objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(objUrcrmusergetdetailsOutput.hs_object_id.ToString());
                                    }
                                    else
                                    {
                                        objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(vid);
                                    }

                                    objSyncUpdateContactIDInput.iccid = objUrcrmusergetdetailsOutput.iccid;
                                    objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;
                                    List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                    try
                                    {
                                        objSyncUpdateContactIDResult = GetSyncCustomersupdate(objSyncUpdateContactIDInput);
                                    }
                                    catch (Exception ex)
                                    {
                                        Write("DB Update Error :" + ex.Message.ToString());
                                    }
                                    #endregion
                                }
                                //}
                                catch (WebException ex)
                                { 
                                    try
                                    {
                                        Write("Error at count:-" + i);
                                        Write("URL:-" + UPDATELIVEURL);
                                        Write("URL:-" + Url); 
                                        using (WebResponse response = ex.Response)
                                        { 
                                            Write("Response exception:" + ex.Response);
                                            HttpWebResponse httpResponse = (HttpWebResponse)response;
                                            Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                                            using (Stream dataException = response.GetResponseStream())
                                            using (var reader = new StreamReader(dataException))
                                            {
                                                var text = reader.ReadToEnd();

                                                Write("Error records:-" + text.ToString());
                                                //Assigning Unique values for update
                                                UpdateContactObject dataResult = JsonConvert.DeserializeObject<UpdateContactObject>(text);
                                                //Call update contact API
                                                int vid = 0;
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(objUrcrmusergetdetailsOutput.hs_object_id)
                                                              && checkEmailHubspot(objUrcrmusergetdetailsOutput.hs_object_id))
                                                    {
                                                        vid = dataResult.identityProfile.vid;
                                                    }
                                                    else
                                                    {
                                                        //Create Contact with Email ID + 1

                                                        int count = 1;
                                                        string resultemailID = "";
                                                    v1:
                                                        string URL = ConfigurationManager.AppSettings["SEARCHURL"].ToString();
                                                        URL = string.Format(URL, objUrcrmusergetdetailsOutput.email.Split('@')[0].ToString() + "+" + count.ToString() + "@" + objUrcrmusergetdetailsOutput.email.Split('@')[1].ToString());
                                                        resultemailID = objUrcrmusergetdetailsOutput.email.Split('@')[0].ToString() + "+" + count.ToString() + "@" + objUrcrmusergetdetailsOutput.email.Split('@')[1].ToString();
                                                        HttpWebRequest httpupdateReq = (HttpWebRequest)WebRequest.Create(URL);
                                                        httpupdateReq.ProtocolVersion = HttpVersion.Version11;
                                                        httpupdateReq.Method = "GET";
                                                        try
                                                        {
                                                            HttpWebResponse Updateresponse = (HttpWebResponse)httpupdateReq.GetResponse();
                                                            string s = Updateresponse.ToString();
                                                            StreamReader Updatereader = new StreamReader(Updateresponse.GetResponseStream());
                                                            String jsonresponse = "";
                                                            String temp = null;
                                                            while ((temp = Updatereader.ReadLine()) != null)
                                                            {
                                                                jsonresponse += temp;
                                                            }
                                                            //Assigning Unique values for update
                                                            var updatedataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);
                                                            string updatevid = string.Empty;
                                                            string isNew = string.Empty;
                                                            if (updatedataResult != null)
                                                            {
                                                                updatevid = updatedataResult["vid"].Value<string>();
                                                                // isNew = dataResult["isNew"].Value<string>();
                                                            }
                                                            if (string.IsNullOrEmpty(updatevid))
                                                            {

                                                            }
                                                            else
                                                            {
                                                                count = count + 1;
                                                                goto v1;
                                                            }
                                                        }
                                                        catch (WebException ex1)
                                                        {
                                                            using (WebResponse Existsresponse = ex1.Response)
                                                            {
                                                                Write("Existsresponse exception:" + ex1.Response);
                                                                HttpWebResponse Resultresponse = (HttpWebResponse)Existsresponse;
                                                                Console.WriteLine("Error code: {0}", Resultresponse.StatusCode);
                                                                using (Stream Resultdata = Resultresponse.GetResponseStream())
                                                                using (var Resultreader = new StreamReader(Resultdata))
                                                                {
                                                                    string textdata = Resultreader.ReadToEnd();
                                                                    var Result = (JObject)JsonConvert.DeserializeObject(textdata);
                                                                    string errmessage = Result["message"].Value<string>();
                                                                    if (errmessage.ToUpper().ToString().Contains("CONTACT DOES NOT EXIST"))
                                                                    {
                                                                        //Create new contact
                                                                        HttpWebRequest httpContactReq = null;
                                                                        if (checkEmailHubspot(objUrcrmusergetdetailsOutput.hs_object_id))
                                                                        {
                                                                            httpContactReq = (HttpWebRequest)WebRequest.Create(Url);
                                                                        }
                                                                        else
                                                                        {
                                                                            httpContactReq = (HttpWebRequest)WebRequest.Create(UPDATELIVEURL);
                                                                        }
                                                                        Encoding Contactencoding = new UTF8Encoding();
                                                                        objProperties.properties = objList;
                                                                        Properties objPropertiesvalues = new Properties();
                                                                        objPropertiesvalues.properties = new List<Contact>();
                                                                      
                                                                        foreach (Contact objContact in objProperties.properties)
                                                                        {
                                                                            //objUrcrmusergetdetailsOutput.hs_object_id
                                                                            if (objContact.property != "email")
                                                                            {
                                                                                if (objContact.property != "alternate_email_id")
                                                                                   objPropertiesvalues.properties.Add(objContact);
                                                                            }
                                                                            else
                                                                            {
                                                                                Contact email = new Contact();
                                                                                email.property = "email";
                                                                                email.value = resultemailID;
                                                                                objProperties.properties = new List<Contact>();
                                                                                objPropertiesvalues.properties.Add(email);

                                                                                if (!string.IsNullOrEmpty(objUrcrmusergetdetailsOutput.email))
                                                                                {
                                                                                    Contact alternate_email_id = new Contact();
                                                                                    alternate_email_id.property = "alternate_email_id";
                                                                                    alternate_email_id.value = objUrcrmusergetdetailsOutput.email;
                                                                                    objProperties.properties = new List<Contact>();
                                                                                    objPropertiesvalues.properties.Add(alternate_email_id);
                                                                                }
                                                                            }
                                                                        }

                                                                        httpContactReq.ProtocolVersion = HttpVersion.Version11;
                                                                        httpContactReq.Method = "POST";
                                                                        httpContactReq.ContentType = "application/json";

                                                                        string postContactData = JsonConvert.SerializeObject(objPropertiesvalues);
                                                                        byte[] dataContent = Contactencoding.GetBytes(postContactData);

                                                                        Stream stream = httpContactReq.GetRequestStream();
                                                                        stream.Write(dataContent, 0, dataContent.Length);
                                                                        stream.Close();


                                                                        HttpWebResponse Contactresponse = (HttpWebResponse)httpContactReq.GetResponse();
                                                                        string s = Contactresponse.ToString();
                                                                        StreamReader Contactreader = new StreamReader(Contactresponse.GetResponseStream());
                                                                        String jsonresponse = "";
                                                                        String temp = null;

                                                                        var textresult = Contactreader.ReadToEnd();

                                                                        while ((temp = Contactreader.ReadLine()) != null)
                                                                        {
                                                                            jsonresponse += temp;
                                                                        }
                                                                        //Assigning Unique values for update
                                                                        //var dataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);
                                                                        var dataContactResult = (JObject)JsonConvert.DeserializeObject(textresult);
                                                                        string vContactid = string.Empty;
                                                                        string isNew = string.Empty;
                                                                        if (dataResult != null)
                                                                        {
                                                                            try
                                                                            {
                                                                                vContactid = dataContactResult["vid"].Value<string>();

                                                                                SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                                                                objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(vContactid);
                                                                                objSyncUpdateContactIDInput.iccid = objUrcrmusergetdetailsOutput.iccid;
                                                                                objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;
                                                                                List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                                                                objSyncUpdateContactIDResult = GetSyncCustomersupdate(objSyncUpdateContactIDInput);
                                                                                //SendMail("New Email ID :" + resultemailID + ":::Contact ID :" + vContactid + ":::Alternate Email ID:" + objUrcrmusergetdetailsOutput.email, objUrcrmusergetdetailsOutput.phone, "Hubspot UK: New contact created", "dotnetteam@vectone.com", "phpteam@vectone.com", "y.abdulrasul@vectone.com", "DBdevteam@vectone.com", "p.loganathan@vectone.com", "s.devendran@vectone.com", "m.manokaran@vectone.com");
                                                                            }
                                                                            catch (Exception ex12)
                                                                            {

                                                                                if (checkEmailHubspot(objUrcrmusergetdetailsOutput.hs_object_id))
                                                                                {
                                                                                    SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                                                                    objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(objUrcrmusergetdetailsOutput.hs_object_id);
                                                                                    objSyncUpdateContactIDInput.iccid = objUrcrmusergetdetailsOutput.iccid;
                                                                                    objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;
                                                                                    List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                                                                    objSyncUpdateContactIDResult = GetSyncCustomersupdate(objSyncUpdateContactIDInput);
                                                                                }
                                                                            }
                                                                           
                                                                        } 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                                catch (Exception)
                                                {

                                                    if (text.Contains("INVALID_EMAIL"))
                                                    {
                                                        SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                                        objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;
                                                        objSyncCustomersInput.contact_id = Convert.ToInt32(objUrcrmusergetdetailsOutput.hs_object_id);
                                                        List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                                        if (dataResult != null)
                                                        {
                                                            objSyncUpdateContactIDInput.contact_id = objSyncCustomersInput.contact_id;
                                                            objSyncUpdateContactIDInput.iccid = objSyncCustomersInput.iccid;
                                                            objSyncUpdateContactIDResult = GetSyncCustomersupdate(objSyncUpdateContactIDInput);
                                                        }

                                                    }
                                                    else if (text.Contains("OBJECT_NOT_FOUND"))
                                                    {
                                                        try
                                                        {
                                                            SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                                            objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;
                                                            objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(objUrcrmusergetdetailsOutput.hs_object_id);
                                                            List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                                            objSyncUpdateContactIDResult = GetSyncCustomersdeletecontact(objSyncUpdateContactIDInput);
                                                        }
                                                        catch (Exception ex1)
                                                        {


                                                        }
                                                    }
                                                    else
                                                    {
                                                        SendMail(text, objUrcrmusergetdetailsOutput.phone, "Hubspot DB Error", "dotnetteam@vectone.com", "phpteam@vectone.com", "y.abdulrasul@vectone.com", "DBdevteam@vectone.com", "p.loganathan@vectone.com", "s.devendran@vectone.com", "m.manokaran@vectone.com");
                                                    }
                                                }

                                                //UPDATELIVEURL 
                                                if (vid > 0)
                                                {
                                                    #region Update Records

                                                    Properties objPropertiesvalues = new Properties();


                                                    if (text.Contains("CONTACT_EXISTS"))
                                                    {
                                                        objPropertiesvalues.properties = new List<Contact>();
                                                        //objProperties.properties.RemoveAt(10); 
                                                        foreach (Contact objContact in objProperties.properties)
                                                        {
                                                            //objUrcrmusergetdetailsOutput.hs_object_id
                                                            if (objContact.property != "email")
                                                            {

                                                                objPropertiesvalues.properties.Add(objContact);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        objPropertiesvalues = objProperties;
                                                    }

                                                    string postUpdateData = JsonConvert.SerializeObject(objPropertiesvalues);
                                                    byte[] dataUpdate = encoding.GetBytes(postUpdateData);
                                                    UPDATELIVEURL = UPDATELIVEURL.Replace("{0}", vid.ToString());
                                                    HttpWebRequest httpupdateReq = (HttpWebRequest)WebRequest.Create(UPDATELIVEURL);
                                                    httpupdateReq.ProtocolVersion = HttpVersion.Version11;
                                                    httpupdateReq.Method = "POST";
                                                    httpupdateReq.ContentType = "application/json";

                                                    Stream streamupdate = httpupdateReq.GetRequestStream();
                                                    streamupdate.Write(dataUpdate, 0, dataUpdate.Length);
                                                    streamupdate.Close();
                                                    try
                                                    {
                                                        HttpWebResponse Updateresponse = (HttpWebResponse)httpupdateReq.GetResponse();
                                                        string s = Updateresponse.ToString();
                                                        StreamReader Updatereader = new StreamReader(Updateresponse.GetResponseStream());
                                                        String jsonresponse = "";
                                                        String temp = null;
                                                        while ((temp = Updatereader.ReadLine()) != null)
                                                        {
                                                            jsonresponse += temp;
                                                        }
                                                        //Assigning Unique values for update
                                                        var updatedataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);
                                                    }
                                                    catch (Exception exception)
                                                    {

                                                    }

                                                    SyncCustomersInput objSyncUpdateContactIDInput = new SyncCustomersInput();
                                                    objSyncUpdateContactIDInput.mobileno = objUrcrmusergetdetailsOutput.phone;

                                                    List<UrcrmusergetdetailsOutput1> objSyncUpdateContactIDResult = null;
                                                    if (dataResult != null)
                                                    {

                                                        if (text.Contains("CONTACT_EXISTS"))
                                                        {
                                                            objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(vid);
                                                        }
                                                        else
                                                        {
                                                            objSyncUpdateContactIDInput.contact_id = Convert.ToInt32(objUrcrmusergetdetailsOutput.hs_object_id);
                                                        }

                                                        objSyncUpdateContactIDInput.iccid = objSyncUpdateContactIDInput.iccid;
                                                        objSyncUpdateContactIDResult = GetSyncCustomersupdate(objSyncUpdateContactIDInput);
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }

                                    }
                                    catch (Exception ex1)
                                    {
                                        Write("Error at count:-" + i);
                                        Write("Record Exception:" + ex1.Message.ToString());

                                        if (ex1.Message.ToString().Contains("Object reference not set to an instance of an object."))
                                        {
                                            if (i > 500)//Temp fix
                                                return;
                                        }
                                    }
                                }
                                #endregion

                                WriteStartEnd("Completed count:-" + i);
                                i = i + 1;
                                UPDATELIVEURL = string.Empty;

                                //}
                                // }
                            }
                        }
                        catch (WebException ex)
                        {
                            try
                            {
                                using (WebResponse response = ex.Response)
                                {
                                    Write("Response exception:" + ex.Response);
                                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                                    using (Stream data = response.GetResponseStream())
                                    using (var reader = new StreamReader(data))
                                    {
                                        string text = reader.ReadToEnd();
                                        Console.WriteLine(text);
                                    }
                                }
                            }
                            catch (Exception ex1)
                            {

                                Write("Outer Exception:" + ex1.Message.ToString());
                            }
                        }

                    }


                }
                else if (scenario == "1")
                {
                    #region UpdateContact
                    //Update//
                    string CREATEUPDATEURL = ConfigurationManager.AppSettings["CREATEUPDATEURL"].ToString();
                    UrcrmusergetdetailsInput objUrcrmusergetdetailsUpdateInput = new UrcrmusergetdetailsInput();
                    objUrcrmusergetdetailsUpdateInput.fromdate = DateTime.Now.ToString("yyyy-MM-dd");
                    objUrcrmusergetdetailsUpdateInput.todate = DateTime.Now.AddMonths(1).ToString("yyyy-MM-dd");

                    List<UrcrmusergetdetailsOutput> objUpdateResult = GetActiveCustomers(objUrcrmusergetdetailsUpdateInput);

                    string objResultUpdateJSONData = JsonConvert.SerializeObject(objUpdateResult);
                    Properties objUpdateProperties = new Properties();
                    List<Contact> objUpdateList = new List<Contact>();

                    if (objUpdateResult.Count > 0 && objUpdateResult[0].errcode != -1)
                    {
                        foreach (UrcrmusergetdetailsOutput objUrcrmusergetdetailsUpdateOutput in objUpdateResult)
                        {
                            //Assigning Unique values
                            CREATEUPDATEURL = CREATEUPDATEURL.Replace("{0}", "email").Replace("{1}", objUrcrmusergetdetailsUpdateOutput.email);

                            CreateProperties(objUrcrmusergetdetailsUpdateOutput, out objUpdateList);
                            Encoding encoding = new UTF8Encoding();
                            objUpdateProperties.properties = objUpdateList;
                            string postData = JsonConvert.SerializeObject(objUpdateProperties);
                            byte[] data = encoding.GetBytes(postData);
                            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(CREATEUPDATEURL);
                            httpWReq.ProtocolVersion = HttpVersion.Version11;
                            httpWReq.Method = "POST";
                            httpWReq.ContentType = "application/json";

                            Stream stream = httpWReq.GetRequestStream();
                            stream.Write(data, 0, data.Length);
                            stream.Close();
                            try
                            {
                                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                                string s = response.ToString();
                                StreamReader reader = new StreamReader(response.GetResponseStream());
                                String jsonresponse = "";
                                String temp = null;
                                while ((temp = reader.ReadLine()) != null)
                                {
                                    jsonresponse += temp;
                                }
                            }
                            catch (Exception ex)
                            {


                            }

                        }

                    }
                    #endregion
                }
            }
            catch (WebException ex)
            {

                Write("Exception : " + ex.Message.ToString());
                //using (WebResponse response = ex.Response)
                //{
                //    Write("Response exception:" + ex.Response);etsys
                //    HttpWebResponse httpResponse = (HttpWebResponse)response;
                //    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                //    using (Stream data = response.GetResponseStream())
                //    using (var reader = new StreamReader(data))
                //    {
                //        string text = reader.ReadToEnd();
                //        Console.WriteLine(text);
                //    }
                //}
            }
            Write("End.." + DateTime.Now.ToString());
        }
        private static void SendMail
            (string payload, string msisdn, string subject, string toaddress, string toaddress1, string toaddress2, string toaddress3, string toaddress4,
            string toaddress5, string toaddress6)
        {


            try
            {
                string from = ConfigurationManager.AppSettings["FromList"];
                string mailContent = payload;
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(toaddress);
                mail.To.Add(toaddress1);
                mail.To.Add(toaddress2);
                mail.To.Add(toaddress3);
                mail.To.Add(toaddress4);
                mail.To.Add(toaddress5);
                mail.To.Add(toaddress6);
                mail.From = new MailAddress(from, "Hubspot Dev");
                mail.Subject = subject + ":" + msisdn;
                mail.IsBodyHtml = true;
                mail.Body = mailContent;
                // mail.BodyEncoding = Encoding.UTF8;
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"]);
                emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);
                emailClient.Send(mail);
            }
            catch (Exception ex)
            {


            }
        }
        public static string CallHubSpot(byte[] data, HttpWebRequest httpWReq, string visitorid)
        {
            try
            {
                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";
                using (Stream reqStream = httpWReq.GetRequestStream())
                {
                    //if (reqStream == null)
                    //{
                    //    return;
                    //}

                    //else
                    //{
                    reqStream.Write(data, 0, data.Length);
                    reqStream.Close();

                    HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                    string s = response.ToString();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    String jsonresponse = "";
                    String temp = null;
                    while ((temp = reader.ReadLine()) != null)
                    {
                        jsonresponse += temp;
                    }
                    //Assigning Unique values for update
                    var dataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);

                    string isNew = string.Empty;
                    if (dataResult != null)
                    {
                        visitorid = dataResult["vid"].Value<string>();
                        // isNew = dataResult["isNew"].Value<string>();
                    }
                    // }

                }
            }
            catch (Exception ex)
            {


            }

            return visitorid;
        }
        public static double convertDateTimeToEpoch(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }
        public static double convertDateTimeToEpoch1(DateTime date)
        {
            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long milliseconds;
            milliseconds = (long)(date.ToUniversalTime() - UnixEpoch).TotalMilliseconds;
            return milliseconds;
        }

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddSeconds(timestamp);
        }
        private static void Write(string message)
        {
            string filePath = "";
            if (ConfigurationManager.AppSettings["LogRequired"] == "1")
            {
                filePath = ConfigurationManager.AppSettings["LOGFOLDERPATH"] + DateTime.Now.ToString("ddMMyyyy") + ".txt";
                File.AppendAllText(filePath, message + Environment.NewLine);
            }
        }
        private static void WriteStartEnd(string message)
        {
            string filePath = "";

            filePath = ConfigurationManager.AppSettings["LOGFOLDERPATH"] + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            File.AppendAllText(filePath, message + Environment.NewLine);

        }
        private static void CreateProperties1(UrcrmusergetdetailsOutput2 objResult, out List<Contact> objList)
        {
            Properties objProperties = new Properties();
            objList = new List<Contact>();
            //Datetime
            if (!string.IsNullOrEmpty(objResult.auto_topup) && !string.IsNullOrEmpty(objResult.auto_topup.ToString()))
            {
                Contact auto_topup = new Contact();
                auto_topup.property = "auto_topup";
                auto_topup.value = (objResult.auto_topup).ToString().Trim();
                objList.Add(auto_topup);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.auto_topup_disable_by))
            {
                Contact auto_topup_disable_by = new Contact();
                auto_topup_disable_by.property = "auto_topup_disable_by";
                auto_topup_disable_by.value = objResult.auto_topup_disable_by;
                objProperties.properties = new List<Contact>();
                objList.Add(auto_topup_disable_by);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.auto_topup_disable_date.ToString()))
            {
                Contact auto_topup_disable_date = new Contact();
                auto_topup_disable_date.property = "auto_topup_disable_date";
                auto_topup_disable_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.auto_topup_disable_date);
                objList.Add(auto_topup_disable_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.auto_topup_enable_by))
            {
                Contact auto_topup_enable_by = new Contact();
                auto_topup_enable_by.property = "auto_topup_enable_by";
                auto_topup_enable_by.value = objResult.auto_topup_enable_by;
                objProperties.properties = new List<Contact>();
                objList.Add(auto_topup_enable_by);
            }
            //int
            if (objResult.current_roaming_bundle_id > 0)
            {
                Contact current_roaming_bundle_id = new Contact();
                current_roaming_bundle_id.property = "current_roaming_bundle_id";
                current_roaming_bundle_id.value = objResult.current_roaming_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_roaming_bundle_id);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.auto_topup_enable_date.ToString()))
            {
                Contact auto_topup_enable_date = new Contact();
                auto_topup_enable_date.property = "auto_topup_enable_date";
                auto_topup_enable_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.auto_topup_enable_date);
                objList.Add(auto_topup_enable_date);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_roaming_bundle_enddate.ToString()))
            {
                Contact current_roaming_bundle_enddate = new Contact();
                current_roaming_bundle_enddate.property = "current_roaming_bundle_enddate";
                current_roaming_bundle_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_roaming_bundle_enddate);
                objList.Add(current_roaming_bundle_enddate);
            }
            ////int
            //if (objResult.no_of_topup != null)
            //{
            //    Contact no_of_topup = new Contact();
            //    no_of_topup.property = "no_of_topup";
            //    no_of_topup.value = objResult.no_of_topup.ToString().Trim();
            //    objList.Add(no_of_topup);
            //}
            //double
            if (objResult.first_topup_amount != null)
            {
                Contact first_topup_amount = new Contact();
                first_topup_amount.property = "first_topup_amount";
                first_topup_amount.value = objResult.first_topup_amount.ToString().Trim();
                objList.Add(first_topup_amount);
            }
            //string
            //Temp Commented
            if (!string.IsNullOrEmpty(objResult.first_topup_method_used))
            {
                Contact first_topup_method_used = new Contact();
                first_topup_method_used.property = "first_topup_method_used";
                first_topup_method_used.value = objResult.first_topup_method_used;
                objProperties.properties = new List<Contact>();
                objList.Add(first_topup_method_used);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.first_time_topup_date.ToString()))
            {
                Contact first_time_topup_date = new Contact();
                first_time_topup_date.property = "first_time_topup_date";
                first_time_topup_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.first_time_topup_date);
                objList.Add(first_time_topup_date);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.latest_auto_topup_attempt_date.ToString()))
            {
                Contact latest_auto_topup_attempt_date = new Contact();
                latest_auto_topup_attempt_date.property = "latest_auto_topup_attempt_date";
                latest_auto_topup_attempt_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.latest_auto_topup_attempt_date);
                objList.Add(latest_auto_topup_attempt_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_auto_topup_attempt_failure_reason))
            {
                Contact latest_auto_topup_attempt_failure_reason = new Contact();
                latest_auto_topup_attempt_failure_reason.property = "latest_auto_topup_attempt_failure_reason";
                latest_auto_topup_attempt_failure_reason.value = objResult.latest_auto_topup_attempt_failure_reason;
                objList.Add(latest_auto_topup_attempt_failure_reason);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_auto_topup_attempt_status))
            {
                Contact latest_auto_topup_attempt_status = new Contact();
                latest_auto_topup_attempt_status.property = "latest_auto_topup_attempt_status";
                latest_auto_topup_attempt_status.value = objResult.latest_auto_topup_attempt_status;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_auto_topup_attempt_status);
            }

            //string            
            if (objResult.latest_topup_amount > 0.0)
            {
                Contact latest_topup_amount = new Contact();
                latest_topup_amount.property = "latest_topup_amount";
                latest_topup_amount.value = objResult.latest_topup_amount.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_amount);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_topup_type))
            {
                Contact latest_topup_type = new Contact();
                latest_topup_type.property = "latest_topup_type";
                latest_topup_type.value = objResult.latest_topup_type;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_type);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.iccid))
            {
                Contact iccid = new Contact();
                iccid.property = "iccid";
                iccid.value = objResult.iccid;
                objProperties.properties = new List<Contact>();
                objList.Add(iccid);
            }

            //Datetime
            if (!string.IsNullOrEmpty(objResult.sim_dispatch_date.ToString()))
            {
                Contact sim_dispatch_date = new Contact();
                sim_dispatch_date.property = "sim_dispatch_date";
                sim_dispatch_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.sim_dispatch_date);
                objList.Add(sim_dispatch_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.sim_inserted) && !string.IsNullOrEmpty(objResult.sim_inserted.ToString()))
            {
                Contact sim_inserted = new Contact();
                sim_inserted.property = "sim_inserted";
                sim_inserted.value = (objResult.sim_inserted).ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(sim_inserted);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.sim_type))
            {
                Contact sim_type = new Contact();
                sim_type.property = "sim_type";
                sim_type.value = objResult.sim_type;
                objProperties.properties = new List<Contact>();
                objList.Add(sim_type);
            }
            //string
            if (objResult.freesimid > 0)
            {
                Contact freesimid = new Contact();
                freesimid.property = "freesimid";
                freesimid.value = objResult.freesimid.ToString().Trim();
                objList.Add(freesimid);
            }
            //int
            if (objResult.subscriberid > 0)
            {
                Contact subscriberid = new Contact();
                subscriberid.property = "subscriberid";
                subscriberid.value = objResult.subscriberid.ToString().Trim();
                objList.Add(subscriberid);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.firstname))
            {
                Contact firstname = new Contact();
                firstname.property = "firstname";
                firstname.value = objResult.firstname;
                objProperties.properties = new List<Contact>();
                objList.Add(firstname);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.lastname))
            {
                Contact lastname = new Contact();
                lastname.property = "lastname";
                lastname.value = objResult.lastname;
                objProperties.properties = new List<Contact>();
                objList.Add(lastname);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.email) && IsValidEmail(objResult.email))
            {
                Contact email = new Contact();
                email.property = "email";
                email.value = objResult.email.ToLower().ToString();
                objProperties.properties = new List<Contact>();
                objList.Add(email);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.houseno))
            {
                Contact houseno = new Contact();
                houseno.property = "houseno";
                houseno.value = objResult.houseno;
                objProperties.properties = new List<Contact>();
                objList.Add(houseno);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.address1))
            {
                Contact address1 = new Contact();
                address1.property = "address1";
                address1.value = objResult.address1;
                objProperties.properties = new List<Contact>();
                objList.Add(address1);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.address2))
            {
                Contact address2 = new Contact();
                address2.property = "address2";
                address2.value = objResult.address2;
                objProperties.properties = new List<Contact>();
                objList.Add(address2);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.city))
            {
                Contact city = new Contact();
                city.property = "city";
                city.value = objResult.city;
                objProperties.properties = new List<Contact>();
                objList.Add(city);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.state))
            {
                Contact state = new Contact();
                state.property = "state";
                state.value = objResult.state;
                objProperties.properties = new List<Contact>();
                objList.Add(state);
            }

            if (!string.IsNullOrEmpty(objResult.mobilephone))
            {
                Contact mobilephone = new Contact();
                mobilephone.property = "mobilephone";
                mobilephone.value = objResult.mobilephone;
                objProperties.properties = new List<Contact>();
                objList.Add(mobilephone);
            }

            //string
            if (objResult.number_of_bundles_purchased > 0)
            {
                Contact number_of_bundles_purchased = new Contact();
                number_of_bundles_purchased.property = "number_of_bundles_purchased";
                number_of_bundles_purchased.value = objResult.number_of_bundles_purchased.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(number_of_bundles_purchased);
            }
            //string
            if (objResult.number_of_active_bundles > 0)
            {
                Contact number_of_active_bundles = new Contact();
                number_of_active_bundles.property = "number_of_active_bundles";
                number_of_active_bundles.value = objResult.number_of_active_bundles.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(number_of_active_bundles);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.first_bundle_name))
            {
                Contact first_bundle_name = new Contact();
                first_bundle_name.property = "first_bundle_name";
                first_bundle_name.value = objResult.first_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(first_bundle_name);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.first_time_purchased_bundle_category))
            {
                Contact first_time_purchased_bundle_category = new Contact();
                first_time_purchased_bundle_category.property = "first_time_purchased_bundle_category";
                first_time_purchased_bundle_category.value = objResult.first_time_purchased_bundle_category;
                objProperties.properties = new List<Contact>();
                objList.Add(first_time_purchased_bundle_category);
            }
            //string
            if (objResult.first_bundle_purchase_amount > 0)
            {
                Contact first_bundle_purchase_amount = new Contact();
                first_bundle_purchase_amount.property = "first_bundle_purchase_amount";
                first_bundle_purchase_amount.value = objResult.first_bundle_purchase_amount.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(first_bundle_purchase_amount);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.first_bundle_purchase_date.ToString()))
            {
                Contact first_bundle_purchase_date = new Contact();
                first_bundle_purchase_date.property = "first_bundle_purchase_date";
                first_bundle_purchase_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.first_bundle_purchase_date);
                objList.Add(first_bundle_purchase_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.first_bundle_purchase_method))
            {
                Contact first_bundle_purchase_method = new Contact();
                first_bundle_purchase_method.property = "first_bundle_purchase_method";
                first_bundle_purchase_method.value = objResult.first_bundle_purchase_method;
                objProperties.properties = new List<Contact>();
                objList.Add(first_bundle_purchase_method);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.first_time_bundle_purchase_payment_method_used))
            {
                Contact first_time_bundle_purchase_payment_method_used = new Contact();
                first_time_bundle_purchase_payment_method_used.property = "first_time_bundle_purchase_payment_method_used";
                first_time_bundle_purchase_payment_method_used.value = objResult.first_time_bundle_purchase_payment_method_used;
                objProperties.properties = new List<Contact>();
                objList.Add(first_time_bundle_purchase_payment_method_used);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.first_bundle_status))
            {
                Contact first_bundle_status = new Contact();
                first_bundle_status.property = "first_bundle_status";
                first_bundle_status.value = objResult.first_bundle_status;
                objProperties.properties = new List<Contact>();
                objList.Add(first_bundle_status);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.latest_bundle_renewal_attempt_date.ToString()))
            {
                Contact latest_bundle_renewal_attempt_date = new Contact();
                latest_bundle_renewal_attempt_date.property = "latest_bundle_renewal_attempt_date";
                latest_bundle_renewal_attempt_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.latest_bundle_renewal_attempt_date);
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_renewal_attempt_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_renewal_attempt_failure_reason))
            {
                Contact latest_bundle_renewal_attempt_failure_reason = new Contact();
                latest_bundle_renewal_attempt_failure_reason.property = "latest_bundle_renewal_attempt_failure_reason";
                latest_bundle_renewal_attempt_failure_reason.value = objResult.latest_bundle_renewal_attempt_failure_reason;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_renewal_attempt_failure_reason);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_renewal_attempt_status))
            {
                Contact latest_bundle_renewal_attempt_status = new Contact();
                latest_bundle_renewal_attempt_status.property = "latest_bundle_renewal_attempt_status";
                latest_bundle_renewal_attempt_status.value = objResult.latest_bundle_renewal_attempt_status;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_renewal_attempt_status);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_name_being_purchased))
            {
                Contact latest_bundle_name_being_purchased = new Contact();
                latest_bundle_name_being_purchased.property = "latest_bundle_name_being_purchased";
                latest_bundle_name_being_purchased.value = objResult.latest_bundle_name_being_purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_name_being_purchased);
            }
            //string
            if (objResult.National_addon_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact National_addon_Bundle_national_mins_total_allowance = new Contact();
                National_addon_Bundle_national_mins_total_allowance.property = "National_addon_Bundle_national_mins_total_allowance";
                National_addon_Bundle_national_mins_total_allowance.value = objResult.National_addon_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_addon_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.National_Bundle_data_allowance > 0.0)
            {
                Contact National_Bundle_data_allowance = new Contact();
                National_Bundle_data_allowance.property = "National_Bundle_data_allowance";
                National_Bundle_data_allowance.value = objResult.National_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_data_allowance);
            }
            //string
            if (objResult.National_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact National_Bundle_data_used_allowance_percentage = new Contact();
                National_Bundle_data_used_allowance_percentage.property = "National_Bundle_data_used_allowance_percentage";
                National_Bundle_data_used_allowance_percentage.value = objResult.National_Bundle_data_used_allowance_percentage.ToString();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_data_used_allowance_percentage);
            }
            //string
            if (objResult.National_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact National_Bundle_international_mins_total_allowance = new Contact();
                National_Bundle_international_mins_total_allowance.property = "National_Bundle_international_mins_total_allowance";
                National_Bundle_international_mins_total_allowance.value = objResult.National_Bundle_international_mins_total_allowance.ToString();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.National_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact National_Bundle_international_mins_used_allowance_percentage = new Contact();
                National_Bundle_international_mins_used_allowance_percentage.property = "National_Bundle_international_mins_used_allowance_percentage";
                National_Bundle_international_mins_used_allowance_percentage.value = objResult.National_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.National_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact National_Bundle_national_mins_total_allowance = new Contact();
                National_Bundle_national_mins_total_allowance.property = "National_Bundle_national_mins_total_allowance";
                National_Bundle_national_mins_total_allowance.value = objResult.National_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.National_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact National_Bundle_national_mins_used_allowance_percentage = new Contact();
                National_Bundle_national_mins_used_allowance_percentage.property = "National_Bundle_national_mins_used_allowance_percentage";
                National_Bundle_national_mins_used_allowance_percentage.value = objResult.National_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.National_Bundle_sms_allowance > 0.0)
            {
                Contact National_Bundle_sms_allowance = new Contact();
                National_Bundle_sms_allowance.property = "National_Bundle_sms_allowance";
                National_Bundle_sms_allowance.value = objResult.National_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_sms_allowance);
            }
            //string
            if (objResult.National_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact National_Bundle_sms_used_allowance_percentage = new Contact();
                National_Bundle_sms_used_allowance_percentage.property = "National_Bundle_sms_used_allowance_percentage";
                National_Bundle_sms_used_allowance_percentage.value = objResult.National_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_sms_used_allowance_percentage);
            }
            //string
            if (objResult.National_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact National_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                National_Bundle_vectone_to_vectone_minutes_allowance.property = "National_Bundle_vectone_to_vectone_minutes_allowance";
                National_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.National_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_vectone_to_vectone_minutes_allowance);
            }
            //string
            if (objResult.AIO_Bundle_data_allowance > 0.0)
            {
                Contact AIO_Bundle_data_allowance = new Contact();
                AIO_Bundle_data_allowance.property = "AIO_Bundle_data_allowance";
                AIO_Bundle_data_allowance.value = objResult.AIO_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_data_allowance);
            }
            //string
            if (objResult.AIO_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact AIO_Bundle_data_used_allowance_percentage = new Contact();
                AIO_Bundle_data_used_allowance_percentage.property = "AIO_Bundle_data_used_allowance_percentage";
                AIO_Bundle_data_used_allowance_percentage.value = objResult.AIO_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_data_used_allowance_percentage);
            }
            //string
            if (objResult.AIO_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact AIO_Bundle_international_mins_total_allowance = new Contact();
                AIO_Bundle_international_mins_total_allowance.property = "AIO_Bundle_international_mins_total_allowance";
                AIO_Bundle_international_mins_total_allowance.value = objResult.AIO_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.first_bundle_purchase_amount > 0.0)
            {
                Contact AIO_Bundle_international_mins_used_allowance_percentage = new Contact();
                AIO_Bundle_international_mins_used_allowance_percentage.property = "AIO_Bundle_international_mins_used_allowance_percentage";
                AIO_Bundle_international_mins_used_allowance_percentage.value = objResult.AIO_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.AIO_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact AIO_Bundle_national_mins_total_allowance = new Contact();
                AIO_Bundle_national_mins_total_allowance.property = "AIO_Bundle_national_mins_total_allowance";
                AIO_Bundle_national_mins_total_allowance.value = objResult.AIO_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.AIO_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact AIO_Bundle_national_mins_used_allowance_percentage = new Contact();
                AIO_Bundle_national_mins_used_allowance_percentage.property = "AIO_Bundle_national_mins_used_allowance_percentage";
                AIO_Bundle_national_mins_used_allowance_percentage.value = objResult.AIO_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.AIO_Bundle_sms_allowance > 0.0)
            {
                Contact AIO_Bundle_sms_allowance = new Contact();
                AIO_Bundle_sms_allowance.property = "AIO_Bundle_sms_allowance";
                AIO_Bundle_sms_allowance.value = objResult.AIO_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_sms_allowance);
            }
            //string
            if (objResult.AIO_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact AIO_Bundle_sms_used_allowance_percentage = new Contact();
                AIO_Bundle_sms_used_allowance_percentage.property = "AIO_Bundle_sms_used_allowance_percentage";
                AIO_Bundle_sms_used_allowance_percentage.value = objResult.AIO_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_sms_used_allowance_percentage);
            }
            //string
            if (objResult.AIO_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact AIO_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                AIO_Bundle_vectone_to_vectone_minutes_allowance.property = "AIO_Bundle_vectone_to_vectone_minutes_allowance";
                AIO_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.AIO_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_vectone_to_vectone_minutes_allowance);
            }
            //string
            if (objResult.AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }
            //string
            if (objResult.current_addon_Data_bundle_id > 0)
            {
                Contact current_addon_Data_bundle_id = new Contact();
                current_addon_Data_bundle_id.property = "current_addon_Data_bundle_id";
                current_addon_Data_bundle_id.value = objResult.current_addon_Data_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_Data_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_addon_Data_bundle_name))
            {
                Contact current_addon_Data_bundle_name = new Contact();
                current_addon_Data_bundle_name.property = "current_addon_Data_bundle_name";
                current_addon_Data_bundle_name.value = objResult.current_addon_Data_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_Data_bundle_name);
            }
            //string
            if (objResult.current_addon_international_bundle_id > 0)
            {
                Contact current_addon_international_bundle_id = new Contact();
                current_addon_international_bundle_id.property = "current_addon_international_bundle_id";
                current_addon_international_bundle_id.value = objResult.current_addon_international_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_international_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_addon_international_bundle_name))
            {
                Contact current_addon_international_bundle_name = new Contact();
                current_addon_international_bundle_name.property = "current_addon_international_bundle_name";
                current_addon_international_bundle_name.value = objResult.current_addon_international_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_international_bundle_name);
            }
            //string
            if (objResult.current_addon_national_bundle_id > 0)
            {
                Contact current_addon_national_bundle_id = new Contact();
                current_addon_national_bundle_id.property = "current_addon_national_bundle_id";
                current_addon_national_bundle_id.value = objResult.current_addon_national_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_national_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_addon_national_bundle_name))
            {
                Contact current_addon_national_bundle_name = new Contact();
                current_addon_national_bundle_name.property = "current_addon_national_bundle_name";
                current_addon_national_bundle_name.value = objResult.current_addon_national_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_national_bundle_name);
            }
            //Datetime
            if (objResult.current_AIO_bundle_id > 0)
            {
                Contact current_AIO_bundle_id = new Contact();
                current_AIO_bundle_id.property = "current_AIO_bundle_id";
                current_AIO_bundle_id.value = (objResult.current_AIO_bundle_id).ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_AIO_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_AIO_bundle_name))
            {
                Contact current_AIO_bundle_name = new Contact();
                current_AIO_bundle_name.property = "current_AIO_bundle_name";
                current_AIO_bundle_name.value = objResult.current_AIO_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_AIO_bundle_name);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_AIO_bundle_renewal_date.ToString()))
            {
                Contact current_AIO_bundle_renewal_date = new Contact();
                current_AIO_bundle_renewal_date.property = "current_AIO_bundle_renewal_date";
                current_AIO_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_AIO_bundle_renewal_date);
                objList.Add(current_AIO_bundle_renewal_date);
            }
            //string
            if (objResult.current_Data_bundle_id > 0)
            {
                Contact current_Data_bundle_id = new Contact();
                current_Data_bundle_id.property = "current_Data_bundle_id";
                current_Data_bundle_id.value = objResult.current_Data_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_Data_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_Data_bundle_name))
            {
                Contact current_Data_bundle_name = new Contact();
                current_Data_bundle_name.property = "current_Data_bundle_name";
                current_Data_bundle_name.value = objResult.current_Data_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_Data_bundle_name);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_data_bundle_renewal_date.ToString()))
            {
                Contact current_data_bundle_renewal_date = new Contact();
                current_data_bundle_renewal_date.property = "current_data_bundle_renewal_date";
                current_data_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_data_bundle_renewal_date);
                objProperties.properties = new List<Contact>();
                objList.Add(current_data_bundle_renewal_date);
            }
            //string
            if (objResult.current_international_bundle_id > 0)
            {
                Contact current_international_bundle_id = new Contact();
                current_international_bundle_id.property = "current_international_bundle_id";
                current_international_bundle_id.value = objResult.current_international_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_international_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_international_bundle_name))
            {
                Contact current_international_bundle_name = new Contact();
                current_international_bundle_name.property = "current_international_bundle_name";
                current_international_bundle_name.value = objResult.current_international_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_international_bundle_name);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_International_bundle_renewal_date.ToString()))
            {
                Contact current_International_bundle_renewal_date = new Contact();
                current_International_bundle_renewal_date.property = "current_International_bundle_renewal_date";
                current_International_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_International_bundle_renewal_date);
                objProperties.properties = new List<Contact>();
                objList.Add(current_International_bundle_renewal_date);
            }
            //int
            if (objResult.current_national_bundle_id > 0)
            {
                Contact current_national_bundle_id = new Contact();
                current_national_bundle_id.property = "current_national_bundle_id";
                current_national_bundle_id.value = objResult.current_national_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_national_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_national_bundle_name))
            {
                Contact current_national_bundle_name = new Contact();
                current_national_bundle_name.property = "current_national_bundle_name";
                current_national_bundle_name.value = objResult.current_national_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_national_bundle_name);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_national_bundle_renewal_date.ToString()))
            {
                Contact current_national_bundle_renewal_date = new Contact();
                current_national_bundle_renewal_date.property = "current_national_bundle_renewal_date";
                current_national_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_national_bundle_renewal_date);
                objList.Add(current_national_bundle_renewal_date);
            }
            //string
            if (objResult.current_SIMONLY_bundle_id > 0)
            {
                Contact current_SIMONLY_bundle_id = new Contact();
                current_SIMONLY_bundle_id.property = "current_SIMONLY_bundle_id";
                current_SIMONLY_bundle_id.value = objResult.current_SIMONLY_bundle_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_SIMONLY_bundle_id);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_SIMONLY_bundle_name))
            {
                Contact current_SIMONLY_bundle_name = new Contact();
                current_SIMONLY_bundle_name.property = "current_SIMONLY_bundle_name";
                current_SIMONLY_bundle_name.value = objResult.current_SIMONLY_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_SIMONLY_bundle_name);
            }
            //string
            if (objResult.Data_addon_Bundle_data_allowance > 0.0)
            {
                Contact Data_addon_Bundle_data_allowance = new Contact();
                Data_addon_Bundle_data_allowance.property = "Data_addon_Bundle_data_allowance";
                Data_addon_Bundle_data_allowance.value = objResult.Data_addon_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_addon_Bundle_data_allowance);
            }
            //string
            if (objResult.Data_addon_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact Data_addon_Bundle_data_used_allowance_percentage = new Contact();
                Data_addon_Bundle_data_used_allowance_percentage.property = "Data_addon_Bundle_data_used_allowance_percentage";
                Data_addon_Bundle_data_used_allowance_percentage.value = objResult.Data_addon_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_addon_Bundle_data_used_allowance_percentage);
            }
            ////string
            //if (objResult.Data_addon_Bundle_international_mins_used_allowance_percentage > 0.0)
            //{
            //    Contact Data_addon_Bundle_international_mins_used_allowance_percentage = new Contact();
            //    Data_addon_Bundle_international_mins_used_allowance_percentage.property = "Data_addon_Bundle_international_mins_used_allowance_percentage";
            //    Data_addon_Bundle_international_mins_used_allowance_percentage.value = objResult.Data_addon_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
            //    objProperties.properties = new List<Contact>();
            //    objList.Add(Data_addon_Bundle_international_mins_used_allowance_percentage);
            //}
            //string
            if (objResult.Data_Bundle_data_allowance > 0.0)
            {
                Contact Data_Bundle_data_allowance = new Contact();
                Data_Bundle_data_allowance.property = "Data_Bundle_data_allowance";
                Data_Bundle_data_allowance.value = objResult.Data_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_data_allowance);
            }
            //string
            if (objResult.Data_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact Data_Bundle_data_used_allowance_percentage = new Contact();
                Data_Bundle_data_used_allowance_percentage.property = "Data_Bundle_data_used_allowance_percentage";
                Data_Bundle_data_used_allowance_percentage.value = objResult.Data_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_data_used_allowance_percentage);
            }
            //string
            if (objResult.Data_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact Data_Bundle_international_mins_total_allowance = new Contact();
                Data_Bundle_international_mins_total_allowance.property = "Data_Bundle_international_mins_total_allowance";
                Data_Bundle_international_mins_total_allowance.value = objResult.Data_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.Data_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact Data_Bundle_international_mins_used_allowance_percentage = new Contact();
                Data_Bundle_international_mins_used_allowance_percentage.property = "Data_Bundle_international_mins_used_allowance_percentage";
                Data_Bundle_international_mins_used_allowance_percentage.value = objResult.Data_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.Data_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact Data_Bundle_national_mins_total_allowance = new Contact();
                Data_Bundle_national_mins_total_allowance.property = "Data_Bundle_national_mins_total_allowance";
                Data_Bundle_national_mins_total_allowance.value = objResult.Data_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.Data_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact Data_Bundle_national_mins_used_allowance_percentage = new Contact();
                Data_Bundle_national_mins_used_allowance_percentage.property = "Data_Bundle_national_mins_used_allowance_percentage";
                Data_Bundle_national_mins_used_allowance_percentage.value = objResult.Data_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.Data_Bundle_sms_allowance > 0.0)
            {
                Contact Data_Bundle_sms_allowance = new Contact();
                Data_Bundle_sms_allowance.property = "Data_Bundle_sms_allowance";
                Data_Bundle_sms_allowance.value = objResult.Data_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_sms_allowance);
            }
            //string
            if (objResult.Data_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact Data_Bundle_sms_used_allowance_percentage = new Contact();
                Data_Bundle_sms_used_allowance_percentage.property = "Data_Bundle_sms_used_allowance_percentage";
                Data_Bundle_sms_used_allowance_percentage.value = objResult.Data_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_sms_used_allowance_percentage);
            }
            //string
            if (objResult.Data_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact Data_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                Data_Bundle_vectone_to_vectone_minutes_allowance.property = "Data_Bundle_vectone_to_vectone_minutes_allowance";
                Data_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.Data_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_vectone_to_vectone_minutes_allowance);
            }
            //string
            if (objResult.Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }
            //string
            if (objResult.International_addon_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact International_addon_Bundle_international_mins_total_allowance = new Contact();
                International_addon_Bundle_international_mins_total_allowance.property = "International_addon_Bundle_international_mins_total_allowance";
                International_addon_Bundle_international_mins_total_allowance.value = objResult.International_addon_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_addon_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.International_addon_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact International_addon_Bundle_international_mins_used_allowance_percentage = new Contact();
                International_addon_Bundle_international_mins_used_allowance_percentage.property = "International_addon_Bundle_international_mins_used_allowance_percentage";
                International_addon_Bundle_international_mins_used_allowance_percentage.value = objResult.International_addon_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_addon_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.International_Bundle_data_allowance > 0.0)
            {
                Contact International_Bundle_data_allowance = new Contact();
                International_Bundle_data_allowance.property = "International_Bundle_data_allowance";
                International_Bundle_data_allowance.value = objResult.International_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_data_allowance);
            }
            //string
            if (objResult.International_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact International_Bundle_data_used_allowance_percentage = new Contact();
                International_Bundle_data_used_allowance_percentage.property = "International_Bundle_data_used_allowance_percentage";
                International_Bundle_data_used_allowance_percentage.value = objResult.International_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_data_used_allowance_percentage);
            }
            //string
            if (objResult.International_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact International_Bundle_international_mins_total_allowance = new Contact();
                International_Bundle_international_mins_total_allowance.property = "International_Bundle_international_mins_total_allowance";
                International_Bundle_international_mins_total_allowance.value = objResult.International_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.International_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact International_Bundle_international_mins_used_allowance_percentage = new Contact();
                International_Bundle_international_mins_used_allowance_percentage.property = "International_Bundle_international_mins_used_allowance_percentage";
                International_Bundle_international_mins_used_allowance_percentage.value = objResult.International_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.International_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact International_Bundle_national_mins_total_allowance = new Contact();
                International_Bundle_national_mins_total_allowance.property = "International_Bundle_national_mins_total_allowance";
                International_Bundle_national_mins_total_allowance.value = objResult.International_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.International_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact International_Bundle_national_mins_used_allowance_percentage = new Contact();
                International_Bundle_national_mins_used_allowance_percentage.property = "International_Bundle_national_mins_used_allowance_percentage";
                International_Bundle_national_mins_used_allowance_percentage.value = objResult.International_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.International_Bundle_sms_allowance > 0.0)
            {
                Contact International_Bundle_sms_allowance = new Contact();
                International_Bundle_sms_allowance.property = "International_Bundle_sms_allowance";
                International_Bundle_sms_allowance.value = objResult.International_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_sms_allowance);
            }
            //string
            if (objResult.International_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact International_Bundle_sms_used_allowance_percentage = new Contact();
                International_Bundle_sms_used_allowance_percentage.property = "International_Bundle_sms_used_allowance_percentage";
                International_Bundle_sms_used_allowance_percentage.value = objResult.International_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_sms_used_allowance_percentage);
            }
            //double            
            if (objResult.International_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact International_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                International_Bundle_vectone_to_vectone_minutes_allowance.property = "International_Bundle_vectone_to_vectone_minutes_allowance";
                International_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.International_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objList.Add(International_Bundle_vectone_to_vectone_minutes_allowance);
            }
            //string
            if (objResult.International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_sim_with_bundle_name_being_purchased))
            {
                Contact latest_sim_with_bundle_name_being_purchased = new Contact();
                latest_sim_with_bundle_name_being_purchased.property = "latest_sim_with_bundle_name_being_purchased";
                latest_sim_with_bundle_name_being_purchased.value = objResult.latest_sim_with_bundle_name_being_purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_sim_with_bundle_name_being_purchased);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_sim_only_with_bundle_name_being_purchased))
            {
                Contact latest_sim_only_with_bundle_name_being_purchased = new Contact();
                latest_sim_only_with_bundle_name_being_purchased.property = "latest_sim_only_with_bundle_name_being_purchased";
                latest_sim_only_with_bundle_name_being_purchased.value = objResult.latest_sim_only_with_bundle_name_being_purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_sim_only_with_bundle_name_being_purchased);
            }
            //string
            if (objResult.National_addon_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact National_addon_Bundle_national_mins_used_allowance_percentage = new Contact();
                National_addon_Bundle_national_mins_used_allowance_percentage.property = "National_addon_Bundle_national_mins_used_allowance_percentage";
                National_addon_Bundle_national_mins_used_allowance_percentage.value = objResult.National_addon_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_addon_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }
            //string
            if (objResult.revenue_from_bundle > 0.0)
            {
                Contact revenue_from_bundle = new Contact();
                revenue_from_bundle.property = "revenue_from_bundle";
                revenue_from_bundle.value = objResult.revenue_from_bundle.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(revenue_from_bundle);
            }
            //string
            if (objResult.simonly_Bundle_data_allowance > 0.0)
            {
                Contact simonly_Bundle_data_allowance = new Contact();
                simonly_Bundle_data_allowance.property = "simonly_Bundle_data_allowance";
                simonly_Bundle_data_allowance.value = objResult.simonly_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_data_allowance);
            }
            //string
            if (objResult.simonly_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact simonly_Bundle_data_used_allowance_percentage = new Contact();
                simonly_Bundle_data_used_allowance_percentage.property = "simonly_Bundle_data_used_allowance_percentage";
                simonly_Bundle_data_used_allowance_percentage.value = objResult.simonly_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_data_used_allowance_percentage);
            }
            //string
            if (objResult.simonly_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact simonly_Bundle_international_mins_total_allowance = new Contact();
                simonly_Bundle_international_mins_total_allowance.property = "simonly_Bundle_international_mins_total_allowance";
                simonly_Bundle_international_mins_total_allowance.value = objResult.simonly_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_international_mins_total_allowance);
            }
            //string
            if (objResult.simonly_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact simonly_Bundle_international_mins_used_allowance_percentage = new Contact();
                simonly_Bundle_international_mins_used_allowance_percentage.property = "simonly_Bundle_international_mins_used_allowance_percentage";
                simonly_Bundle_international_mins_used_allowance_percentage.value = objResult.simonly_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_international_mins_used_allowance_percentage);
            }
            //string
            if (objResult.simonly_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact simonly_Bundle_national_mins_total_allowance = new Contact();
                simonly_Bundle_national_mins_total_allowance.property = "simonly_Bundle_national_mins_total_allowance";
                simonly_Bundle_national_mins_total_allowance.value = objResult.simonly_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_national_mins_total_allowance);
            }
            //string
            if (objResult.simonly_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact simonly_Bundle_national_mins_used_allowance_percentage = new Contact();
                simonly_Bundle_national_mins_used_allowance_percentage.property = "simonly_Bundle_national_mins_used_allowance_percentage";
                simonly_Bundle_national_mins_used_allowance_percentage.value = objResult.simonly_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_national_mins_used_allowance_percentage);
            }
            //string
            if (objResult.simonly_Bundle_sms_allowance > 0.0)
            {
                Contact simonly_Bundle_sms_allowance = new Contact();
                simonly_Bundle_sms_allowance.property = "simonly_Bundle_sms_allowance";
                simonly_Bundle_sms_allowance.value = objResult.simonly_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_sms_allowance);
            }
            //string
            if (objResult.simonly_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact simonly_Bundle_sms_used_allowance_percentage = new Contact();
                simonly_Bundle_sms_used_allowance_percentage.property = "simonly_Bundle_sms_used_allowance_percentage";
                simonly_Bundle_sms_used_allowance_percentage.value = objResult.simonly_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_sms_used_allowance_percentage);
            }
            //string
            if (objResult.simonly_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact simonly_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                simonly_Bundle_vectone_to_vectone_minutes_allowance.property = "simonly_Bundle_vectone_to_vectone_minutes_allowance";
                simonly_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.simonly_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_vectone_to_vectone_minutes_allowance);
            }
            //string
            if (objResult.simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }
            //string
            if (objResult.current_national_bundle_most_international_mins_used_country > 0.0)
            {
                Contact current_national_bundle_most_international_mins_used_country = new Contact();
                current_national_bundle_most_international_mins_used_country.property = "current_national_bundle_most_international_mins_used_country";
                current_national_bundle_most_international_mins_used_country.value = objResult.current_national_bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_national_bundle_most_international_mins_used_country);
            }
            //string
            if (objResult.current_international_bundle_most_international_mins_used_country > 0.0)
            {
                Contact current_international_bundle_most_international_mins_used_country = new Contact();
                current_international_bundle_most_international_mins_used_country.property = "current_international_bundle_most_international_mins_used_country";
                current_international_bundle_most_international_mins_used_country.value = objResult.current_international_bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_international_bundle_most_international_mins_used_country);
            }
            //string
            if (objResult.current_AIO_bundle_most_international_mins_used_country > 0.0)
            {
                Contact current_AIO_bundle_most_international_mins_used_country = new Contact();
                current_AIO_bundle_most_international_mins_used_country.property = "current_AIO_bundle_most_international_mins_used_country";
                current_AIO_bundle_most_international_mins_used_country.value = objResult.current_AIO_bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_AIO_bundle_most_international_mins_used_country);
            }
            //string
            if (objResult.current_SIMONly_bundle_most_international_mins_used_country > 0.0)
            {
                Contact current_SIMONly_bundle_most_international_mins_used_country = new Contact();
                current_SIMONly_bundle_most_international_mins_used_country.property = "current_SIMONly_bundle_most_international_mins_used_country";
                current_SIMONly_bundle_most_international_mins_used_country.value = objResult.current_SIMONly_bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_SIMONly_bundle_most_international_mins_used_country);
            }
            //string
            if (objResult.National_Bundle_most_international_mins_used_country_percentage > 0.0)
            {
                Contact National_Bundle_most_international_mins_used_country_percentage = new Contact();
                National_Bundle_most_international_mins_used_country_percentage.property = "National_Bundle_most_international_mins_used_country_percentage";
                National_Bundle_most_international_mins_used_country_percentage.value = objResult.National_Bundle_most_international_mins_used_country_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(National_Bundle_most_international_mins_used_country_percentage);
            }
            //string
            if (objResult.International_Bundle_most_international_mins_used_country_percentage > 0.0)
            {
                Contact International_Bundle_most_international_mins_used_country_percentage = new Contact();
                International_Bundle_most_international_mins_used_country_percentage.property = "International_Bundle_most_international_mins_used_country_percentage";
                International_Bundle_most_international_mins_used_country_percentage.value = objResult.International_Bundle_most_international_mins_used_country_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(International_Bundle_most_international_mins_used_country_percentage);
            }
            //string
            if (objResult.AIO_Bundle_most_international_mins_used_country_percentage > 0.0)
            {
                Contact AIO_Bundle_most_international_mins_used_country_percentage = new Contact();
                AIO_Bundle_most_international_mins_used_country_percentage.property = "AIO_Bundle_most_international_mins_used_country_percentage";
                AIO_Bundle_most_international_mins_used_country_percentage.value = objResult.AIO_Bundle_most_international_mins_used_country_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(AIO_Bundle_most_international_mins_used_country_percentage);
            }
            //string
            if (objResult.Data_Bundle_most_international_mins_used_country > 0.0)
            {
                Contact Data_Bundle_most_international_mins_used_country = new Contact();
                Data_Bundle_most_international_mins_used_country.property = "Data_Bundle_most_international_mins_used_country";
                Data_Bundle_most_international_mins_used_country.value = objResult.Data_Bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_most_international_mins_used_country);
            }
            //string
            if (objResult.Data_Bundle_most_international_mins_used_country_percentage > 0.0)
            {
                Contact Data_Bundle_most_international_mins_used_country_percentage = new Contact();
                Data_Bundle_most_international_mins_used_country_percentage.property = "Data_Bundle_most_international_mins_used_country_percentage";
                Data_Bundle_most_international_mins_used_country_percentage.value = objResult.Data_Bundle_most_international_mins_used_country_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Data_Bundle_most_international_mins_used_country_percentage);
            }
            //string
            if (objResult.current_SIMONly_bundle_most_international_mins_used_country_per > 0.0)
            {
                Contact current_SIMONly_bundle_most_international_mins_used_country_per = new Contact();
                current_SIMONly_bundle_most_international_mins_used_country_per.property = "current_SIMONly_bundle_most_international_mins_used_country_per";
                current_SIMONly_bundle_most_international_mins_used_country_per.value = objResult.current_SIMONly_bundle_most_international_mins_used_country_per.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_SIMONly_bundle_most_international_mins_used_country_per);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.previously_subscribed_for_country_specific_bundles_))
            {
                Contact previously_subscribed_for_country_specific_bundles_ = new Contact();
                previously_subscribed_for_country_specific_bundles_.property = "previously_subscribed_for_country_specific_bundles_";
                previously_subscribed_for_country_specific_bundles_.value = objResult.previously_subscribed_for_country_specific_bundles_;
                objProperties.properties = new List<Contact>();
                objList.Add(previously_subscribed_for_country_specific_bundles_);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.most_subscribed_country_specific_bundle_name))
            {
                Contact most_subscribed_country_specific_bundle_name = new Contact();
                most_subscribed_country_specific_bundle_name.property = "most_subscribed_country_specific_bundle_name";
                most_subscribed_country_specific_bundle_name.value = objResult.most_subscribed_country_specific_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(most_subscribed_country_specific_bundle_name);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_national_bundle))
            {
                Contact Has_active_national_bundle = new Contact();
                Has_active_national_bundle.property = "Has_active_national_bundle";
                Has_active_national_bundle.value = objResult.Has_active_national_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_national_bundle);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_AIO_bundle))
            {
                Contact Has_active_AIO_bundle = new Contact();
                Has_active_AIO_bundle.property = "Has_active_AIO_bundle";
                Has_active_AIO_bundle.value = objResult.Has_active_AIO_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_AIO_bundle);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_International_bundle))
            {
                Contact Has_active_International_bundle = new Contact();
                Has_active_International_bundle.property = "Has_active_International_bundle";
                Has_active_International_bundle.value = objResult.Has_active_International_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_International_bundle);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_data_bundle))
            {
                Contact Has_active_data_bundle = new Contact();
                Has_active_data_bundle.property = "Has_active_data_bundle";
                Has_active_data_bundle.value = objResult.Has_active_data_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_data_bundle);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_roaming_bundle))
            {
                Contact Has_active_roaming_bundle = new Contact();
                Has_active_roaming_bundle.property = "Has_active_roaming_bundle";
                Has_active_roaming_bundle.value = objResult.Has_active_roaming_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_roaming_bundle);
            }

            //string
            if (objResult.latest_bundle_id_being_purchased > 0.0)
            {
                Contact latest_bundle_id_being_purchased = new Contact();
                latest_bundle_id_being_purchased.property = "latest_bundle_id_being_purchased";
                latest_bundle_id_being_purchased.value = objResult.latest_bundle_id_being_purchased.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_id_being_purchased);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_SIMONLY_bundle))
            {
                Contact Has_active_SIMONLY_bundle = new Contact();
                Has_active_SIMONLY_bundle.property = "Has_active_SIMONLY_bundle";
                Has_active_SIMONLY_bundle.value = objResult.Has_active_SIMONLY_bundle;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_SIMONLY_bundle);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_SIMONLY_bundle_renewal_date.ToString()))
            {
                Contact current_SIMONLY_bundle_renewal_date = new Contact();
                current_SIMONLY_bundle_renewal_date.property = "current_SIMONLY_bundle_renewal_date";
                current_SIMONLY_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_SIMONLY_bundle_renewal_date);
                objProperties.properties = new List<Contact>();
                objList.Add(current_SIMONLY_bundle_renewal_date);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.latest_bundle_purchase_date.ToString()))
            {
                Contact latest_bundle_purchase_date = new Contact();
                latest_bundle_purchase_date.property = "latest_bundle_purchase_date";
                latest_bundle_purchase_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.latest_bundle_purchase_date);
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_purchase_date);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_payment_mode))
            {
                Contact latest_bundle_payment_mode = new Contact();
                latest_bundle_payment_mode.property = "latest_bundle_payment_mode";
                latest_bundle_payment_mode.value = objResult.latest_bundle_payment_mode;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_payment_mode);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_payment_method))
            {
                Contact latest_bundle_payment_method = new Contact();
                latest_bundle_payment_method.property = "latest_bundle_payment_method";
                latest_bundle_payment_method.value = objResult.latest_bundle_payment_method;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_payment_method);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_bundle_payment_reference_number))
            {
                Contact latest_bundle_payment_reference_number = new Contact();
                latest_bundle_payment_reference_number.property = "latest_bundle_payment_reference_number";
                latest_bundle_payment_reference_number.value = objResult.latest_bundle_payment_reference_number;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_bundle_payment_reference_number);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_Data_enddate.ToString()))
            {
                Contact current_addon_Data_enddate = new Contact();
                current_addon_Data_enddate.property = "current_addon_Data_enddate";
                current_addon_Data_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_Data_enddate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_Data_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_Data_startdate.ToString()))
            {
                Contact current_addon_Data_startdate = new Contact();
                current_addon_Data_startdate.property = "current_addon_Data_startdate";
                current_addon_Data_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_Data_startdate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_Data_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_international_enddate.ToString()))
            {
                Contact current_addon_international_enddate = new Contact();
                current_addon_international_enddate.property = "current_addon_international_enddate";
                current_addon_international_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_international_enddate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_international_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_international_startdate.ToString()))
            {
                Contact current_addon_international_startdate = new Contact();
                current_addon_international_startdate.property = "current_addon_international_startdate";
                current_addon_international_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_international_startdate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_international_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_national_enddate.ToString()))
            {
                Contact current_addon_national_enddate = new Contact();
                current_addon_national_enddate.property = "current_addon_national_enddate";
                current_addon_national_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_national_enddate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_national_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_addon_national_startdate.ToString()))
            {
                Contact current_addon_national_startdate = new Contact();
                current_addon_national_startdate.property = "current_addon_national_startdate";
                current_addon_national_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_addon_national_startdate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_addon_national_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_AIO_enddate.ToString()))
            {
                Contact current_AIO_enddate = new Contact();
                current_AIO_enddate.property = "current_AIO_enddate";
                current_AIO_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_AIO_enddate);
                objList.Add(current_AIO_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_AIO_startdate.ToString()))
            {
                Contact current_AIO_startdate = new Contact();
                current_AIO_startdate.property = "current_AIO_startdate";
                current_AIO_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_AIO_startdate);
                objList.Add(current_AIO_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_Data_enddate.ToString()))
            {
                Contact current_Data_enddate = new Contact();
                current_Data_enddate.property = "current_Data_enddate";
                current_Data_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_Data_enddate);
                objList.Add(current_Data_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_Data_startdate.ToString()))
            {
                Contact current_Data_startdate = new Contact();
                current_Data_startdate.property = "current_Data_startdate";
                current_Data_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_Data_startdate);
                objList.Add(current_Data_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_international_enddate.ToString()))
            {
                Contact current_international_enddate = new Contact();
                current_international_enddate.property = "current_international_enddate";
                current_international_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_international_enddate);
                objList.Add(current_international_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_international_startdate.ToString()))
            {
                Contact current_international_startdate = new Contact();
                current_international_startdate.property = "current_international_startdate";
                current_international_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_international_startdate);
                objList.Add(current_international_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_national_enddate.ToString()))
            {
                Contact current_national_enddate = new Contact();
                current_national_enddate.property = "current_national_enddate";
                current_national_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_national_enddate);
                objList.Add(current_national_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_national_startdate.ToString()))
            {
                Contact current_national_startdate = new Contact();
                current_national_startdate.property = "current_national_startdate";
                current_national_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_national_startdate);
                objList.Add(current_national_startdate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_SIMONLY_enddate.ToString()))
            {
                Contact current_SIMONLY_enddate = new Contact();
                current_SIMONLY_enddate.property = "current_SIMONLY_enddate";
                current_SIMONLY_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_SIMONLY_enddate);
                objList.Add(current_SIMONLY_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_SIMONLY_startdate.ToString()))
            {
                Contact current_SIMONLY_startdate = new Contact();
                current_SIMONLY_startdate.property = "current_SIMONLY_startdate";
                current_SIMONLY_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_SIMONLY_startdate);
                objList.Add(current_SIMONLY_startdate);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_data_roaming_addon_name))
            {
                Contact current_data_roaming_addon_name = new Contact();
                current_data_roaming_addon_name.property = "current_data_roaming_addon_name";
                current_data_roaming_addon_name.value = objResult.current_data_roaming_addon_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_data_roaming_addon_name);
            }
            //string
            if (objResult.current_data_roaming_addon_id > 0.0)
            {
                Contact current_data_roaming_addon_id = new Contact();
                current_data_roaming_addon_id.property = "current_data_roaming_addon_id";
                current_data_roaming_addon_id.value = objResult.current_data_roaming_addon_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_data_roaming_addon_id);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.current_data_roaming_addon_startdate.ToString()))
            {
                Contact current_data_roaming_addon_startdate = new Contact();
                current_data_roaming_addon_startdate.property = "current_data_roaming_addon_startdate";
                current_data_roaming_addon_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_data_roaming_addon_startdate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_data_roaming_addon_startdate);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.current_data_roaming_addon_enddate.ToString()))
            {
                Contact current_data_roaming_addon_enddate = new Contact();
                current_data_roaming_addon_enddate.property = "current_data_roaming_addon_enddate";
                current_data_roaming_addon_enddate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_data_roaming_addon_enddate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_data_roaming_addon_enddate);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.data_roaming_addon_data_total_allowance.ToString()))
            {
                Contact data_roaming_addon_data_total_allowance = new Contact();
                data_roaming_addon_data_total_allowance.property = "data_roaming_addon_data_total_allowance";
                data_roaming_addon_data_total_allowance.value = (objResult.data_roaming_addon_data_total_allowance).ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(data_roaming_addon_data_total_allowance);
            }
            //string
            if (objResult.data_roaming_addon_data_used_allowance_percentage > 0.0)
            {
                Contact data_roaming_addon_data_used_allowance_percentage = new Contact();
                data_roaming_addon_data_used_allowance_percentage.property = "data_roaming_addon_data_used_allowance_percentage";
                data_roaming_addon_data_used_allowance_percentage.value = objResult.data_roaming_addon_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(data_roaming_addon_data_used_allowance_percentage);
            }
            //string
            if (objResult.number_of_addons_purchased > 0.0)
            {
                Contact number_of_addons_purchased = new Contact();
                number_of_addons_purchased.property = "number_of_addons_purchased";
                number_of_addons_purchased.value = objResult.number_of_addons_purchased.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(number_of_addons_purchased);
            }
            //string
            if (objResult.number_of_active_addons > 0.0)
            {
                Contact number_of_active_addons = new Contact();
                number_of_active_addons.property = "number_of_active_addons";
                number_of_active_addons.value = objResult.number_of_active_addons.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(number_of_active_addons);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.latest_addon_being_purchased))
            {
                Contact latest_addon_being_purchased = new Contact();
                latest_addon_being_purchased.property = "latest_addon_being_purchased";
                latest_addon_being_purchased.value = objResult.latest_addon_being_purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_addon_being_purchased);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_national_addon))
            {
                Contact Has_active_national_addon = new Contact();
                Has_active_national_addon.property = "Has_active_national_addon";
                Has_active_national_addon.value = objResult.Has_active_national_addon;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_national_addon);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_international_addon))
            {
                Contact Has_active_international_addon = new Contact();
                Has_active_international_addon.property = "Has_active_international_addon";
                Has_active_international_addon.value = objResult.Has_active_international_addon;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_international_addon);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_data_roaming_addon))
            {
                Contact Has_active_data_roaming_addon = new Contact();
                Has_active_data_roaming_addon.property = "Has_active_data_roaming_addon";
                Has_active_data_roaming_addon.value = objResult.Has_active_data_roaming_addon;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_data_roaming_addon);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.Has_active_data_addon))
            {
                Contact Has_active_data_addon = new Contact();
                Has_active_data_addon.property = "Has_active_data_addon";
                Has_active_data_addon.value = objResult.Has_active_data_addon;
                objProperties.properties = new List<Contact>();
                objList.Add(Has_active_data_addon);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.roaming_status))
            {
                Contact roaming_status = new Contact();
                roaming_status.property = "roaming_status";
                roaming_status.value = objResult.roaming_status;
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_status);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.auto_renewal))
            {
                Contact auto_renewal = new Contact();
                auto_renewal.property = "auto_renewal";
                auto_renewal.value = objResult.auto_renewal;
                objProperties.properties = new List<Contact>();
                objList.Add(auto_renewal);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.auto_renewal_disable_by))
            {
                Contact auto_renewal_disable_by = new Contact();
                auto_renewal_disable_by.property = "auto_renewal_disable_by";
                auto_renewal_disable_by.value = objResult.auto_renewal_disable_by;
                objProperties.properties = new List<Contact>();
                objList.Add(auto_renewal_disable_by);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.auto_renewal_disable_date.ToString()))
            {
                Contact auto_renewal_disable_date = new Contact();
                auto_renewal_disable_date.property = "auto_renewal_disable_date";
                auto_renewal_disable_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.auto_renewal_disable_date);
                objProperties.properties = new List<Contact>();
                objList.Add(auto_renewal_disable_date);
            }
            //string            
            if (!string.IsNullOrEmpty(objResult.auto_renewal_enable_by))
            {
                Contact auto_renewal_enable_by = new Contact();
                auto_renewal_enable_by.property = "auto_renewal_enable_by";
                auto_renewal_enable_by.value = objResult.auto_renewal_enable_by;
                objProperties.properties = new List<Contact>();
                objList.Add(auto_renewal_enable_by);
            }
            //Datetime
            if (!string.IsNullOrEmpty(objResult.auto_renewal_enable_date.ToString()))
            {
                Contact auto_renewal_enable_date = new Contact();
                auto_renewal_enable_date.property = "auto_renewal_enable_date";
                auto_renewal_enable_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.auto_renewal_enable_date);
                objProperties.properties = new List<Contact>();
                objList.Add(auto_renewal_enable_date);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.alternate_email_id))
            {
                Contact alternate_email_id = new Contact();
                alternate_email_id.property = "alternate_email_id";
                alternate_email_id.value = objResult.alternate_email_id;
                objProperties.properties = new List<Contact>();
                objList.Add(alternate_email_id);
            }

            ////double
            //if (objResult.arpu > 0.0)
            ////if (!string.IsNullOrEmpty(objResult.arpu))
            //{
            //    Contact arpu = new Contact();
            //    arpu.property = "arpu";
            //    arpu.value = objResult.arpu.ToString().Trim();
            //    objProperties.properties = new List<Contact>();
            //    objList.Add(arpu);
            //}

            //string
            if (objResult.avg_customer_wait_time_between_purchases > 0.0)
            {
                Contact avg_customer_wait_time_between_purchases = new Contact();
                avg_customer_wait_time_between_purchases.property = "avg_customer_wait_time_between_purchases";
                avg_customer_wait_time_between_purchases.value = objResult.avg_customer_wait_time_between_purchases.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(avg_customer_wait_time_between_purchases);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.country))
            {
                Contact country = new Contact();
                country.property = "country";
                country.value = objResult.country;
                objProperties.properties = new List<Contact>();
                objList.Add(country);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.country_code))
            {
                Contact country_code = new Contact();
                country_code.property = "country_code";
                country_code.value = objResult.country_code;
                objProperties.properties = new List<Contact>();
                objList.Add(country_code);
            }

            //string
            if (objResult.current_balance > 0.0)
            {
                Contact current_balance = new Contact();
                current_balance.property = "current_balance";
                current_balance.value = objResult.current_balance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(current_balance);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.current_roaming_bundle_name))
            {
                Contact current_roaming_bundle_name = new Contact();
                current_roaming_bundle_name.property = "current_roaming_bundle_name";
                current_roaming_bundle_name.value = objResult.current_roaming_bundle_name;
                objProperties.properties = new List<Contact>();
                objList.Add(current_roaming_bundle_name);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.current_roaming_bundle_renewal_date.ToString()))
            {
                Contact current_roaming_bundle_renewal_date = new Contact();
                current_roaming_bundle_renewal_date.property = "current_roaming_bundle_renewal_date";
                current_roaming_bundle_renewal_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_roaming_bundle_renewal_date);
                objProperties.properties = new List<Contact>();
                objList.Add(current_roaming_bundle_renewal_date);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.current_roaming_bundle_startdate.ToString()))
            {
                Contact current_roaming_bundle_startdate = new Contact();
                current_roaming_bundle_startdate.property = "current_roaming_bundle_startdate";
                current_roaming_bundle_startdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.current_roaming_bundle_startdate);
                objProperties.properties = new List<Contact>();
                objList.Add(current_roaming_bundle_startdate);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.customer_type))
            {
                Contact customer_type = new Contact();
                customer_type.property = "customer_type";
                customer_type.value = objResult.customer_type;
                objProperties.properties = new List<Contact>();
                objList.Add(customer_type);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.date_of_birth.ToString()))
            {
                Contact date_of_birth = new Contact();
                date_of_birth.property = "date_of_birth";
                date_of_birth.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.date_of_birth);
                objProperties.properties = new List<Contact>();
                objList.Add(date_of_birth);
            }
            //string         
            //Temp Commented
            if (!string.IsNullOrEmpty(objResult.email_verification_status))
            {
                Contact email_verification_status = new Contact();
                email_verification_status.property = "email_verification_status";
                email_verification_status.value = objResult.email_verification_status;
                objProperties.properties = new List<Contact>();
                objList.Add(email_verification_status);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.first_referral_date.ToString()))
            {
                Contact first_referral_date = new Contact();
                first_referral_date.property = "first_referral_date";
                first_referral_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.first_referral_date);
                objProperties.properties = new List<Contact>();
                objList.Add(first_referral_date);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.first_time_topup_payment_method_used))
            {
                Contact first_time_topup_payment_method_used = new Contact();
                first_time_topup_payment_method_used.property = "first_time_topup_payment_method_used";
                first_time_topup_payment_method_used.value = objResult.first_time_topup_payment_method_used;
                objProperties.properties = new List<Contact>();
                objList.Add(first_time_topup_payment_method_used);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.first_topup_status))
            {
                Contact first_topup_status = new Contact();
                first_topup_status.property = "first_topup_status";
                first_topup_status.value = objResult.first_topup_status;
                objProperties.properties = new List<Contact>();
                objList.Add(first_topup_status);
            }

            //string
            if (objResult.free_credit > 0.0)
            {
                Contact free_credit = new Contact();
                free_credit.property = "free_credit";
                free_credit.value = objResult.free_credit.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(free_credit);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.frequent_topup_method))
            {
                Contact frequent_topup_method = new Contact();
                frequent_topup_method.property = "frequent_topup_method";
                frequent_topup_method.value = objResult.frequent_topup_method;
                objProperties.properties = new List<Contact>();
                objList.Add(frequent_topup_method);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.has_llom))
            {
                Contact has_llom = new Contact();
                has_llom.property = "has_llom";
                has_llom.value = objResult.has_llom;
                objProperties.properties = new List<Contact>();
                objList.Add(has_llom);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.has_vectone_extra))
            {
                Contact has_vectone_extra = new Contact();
                has_vectone_extra.property = "has_vectone_extra";
                has_vectone_extra.value = objResult.has_vectone_extra;
                objProperties.properties = new List<Contact>();
                objList.Add(has_vectone_extra);
            }

            //string
            //Temp Commented
            if (!string.IsNullOrEmpty(objResult.hs_analytics_source))
            {
                Contact hs_analytics_source = new Contact();
                hs_analytics_source.property = "hs_analytics_source";
                hs_analytics_source.value = objResult.hs_analytics_source;
                objProperties.properties = new List<Contact>();
                objList.Add(hs_analytics_source);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.hs_language))
            {
                Contact hs_language = new Contact();
                hs_language.property = "hs_language";
                hs_language.value = objResult.hs_language;
                objProperties.properties = new List<Contact>();
                objList.Add(hs_language);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.hs_lead_status))
            {
                Contact hs_lead_status = new Contact();
                hs_lead_status.property = "hs_lead_status";
                hs_lead_status.value = objResult.hs_lead_status;
                objProperties.properties = new List<Contact>();
                objList.Add(hs_lead_status);
            }
            //int           
            if (objResult.hs_object_id != null)
            {
                //Contact hs_object_id = new Contact();
                //hs_object_id.property = "hs_object_id";
                //hs_object_id.value = objResult.hs_object_id.ToString().Trim();
                //objList.Add(hs_object_id);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.international_topup_country_name))
            {
                Contact international_topup_country_name = new Contact();
                international_topup_country_name.property = "international_topup_country_name";
                international_topup_country_name.value = objResult.international_topup_country_name;
                objProperties.properties = new List<Contact>();
                objList.Add(international_topup_country_name);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.international_topup_network_name))
            {
                Contact international_topup_network_name = new Contact();
                international_topup_network_name.property = "international_topup_network_name";
                international_topup_network_name.value = objResult.international_topup_network_name;
                objProperties.properties = new List<Contact>();
                objList.Add(international_topup_network_name);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.last_location_update_date.ToString()))
            {
                Contact last_location_update_date = new Contact();
                last_location_update_date.property = "last_location_update_date";
                last_location_update_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.last_location_update_date);
                objProperties.properties = new List<Contact>();
                objList.Add(last_location_update_date);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.last_referral_date.ToString()))
            {
                Contact last_referral_date = new Contact();
                last_referral_date.property = "last_referral_date";
                last_referral_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.last_referral_date);
                objProperties.properties = new List<Contact>();
                objList.Add(last_referral_date);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.lastcalldate.ToString()))
            {
                Contact lastcalldate = new Contact();
                lastcalldate.property = "lastcalldate";
                lastcalldate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.lastcalldate);
                objProperties.properties = new List<Contact>();
                objList.Add(lastcalldate);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.lastgprsdate.ToString()))
            {
                Contact lastgprsdate = new Contact();
                lastgprsdate.property = "lastgprsdate";
                lastgprsdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.lastgprsdate);
                objProperties.properties = new List<Contact>();
                objList.Add(lastgprsdate);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.lastsmsdate.ToString()))
            {
                Contact lastsmsdate = new Contact();
                lastsmsdate.property = "lastsmsdate";
                lastsmsdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.lastsmsdate);
                objProperties.properties = new List<Contact>();
                objList.Add(lastsmsdate);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_attempt_payment_reference_number))
            {
                Contact latest_attempt_payment_reference_number = new Contact();
                latest_attempt_payment_reference_number.property = "latest_attempt_payment_reference_number";
                latest_attempt_payment_reference_number.value = objResult.latest_attempt_payment_reference_number;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_attempt_payment_reference_number);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_buying_stage))
            {
                Contact latest_buying_stage = new Contact();
                latest_buying_stage.property = "latest_buying_stage";
                latest_buying_stage.value = objResult.latest_buying_stage;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_buying_stage);
            }

            //double
            if (objResult.latest_international_topup_amount > 0.0)
            {
                Contact latest_international_topup_amount = new Contact();
                latest_international_topup_amount.property = "latest_international_topup_amount";
                latest_international_topup_amount.value = objResult.latest_international_topup_amount.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(latest_international_topup_amount);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_international_topup_request_completion_status))
            {
                Contact latest_international_topup_request_completion_status = new Contact();
                latest_international_topup_request_completion_status.property = "latest_international_topup_request_completion_status";
                latest_international_topup_request_completion_status.value = objResult.latest_international_topup_request_completion_status;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_international_topup_request_completion_status);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_international_topup_request_failure_reason))
            {
                Contact latest_international_topup_request_failure_reason = new Contact();
                latest_international_topup_request_failure_reason.property = "latest_international_topup_request_failure_reason";
                latest_international_topup_request_failure_reason.value = objResult.latest_international_topup_request_failure_reason;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_international_topup_request_failure_reason);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_payment_attempt_failure_reason))
            {
                Contact latest_payment_attempt_failure_reason = new Contact();
                latest_payment_attempt_failure_reason.property = "latest_payment_attempt_failure_reason";
                latest_payment_attempt_failure_reason.value = objResult.latest_payment_attempt_failure_reason;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_payment_attempt_failure_reason);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.Latest_Preloaded_SIM_Amount_Being_Purchased))
            {
                Contact Latest_Preloaded_SIM_Amount_Being_Purchased = new Contact();
                Latest_Preloaded_SIM_Amount_Being_Purchased.property = "Latest_Preloaded_SIM_Amount_Being_Purchased";
                Latest_Preloaded_SIM_Amount_Being_Purchased.value = objResult.Latest_Preloaded_SIM_Amount_Being_Purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(Latest_Preloaded_SIM_Amount_Being_Purchased);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_product_being_purchased))
            {
                Contact latest_product_being_purchased = new Contact();
                latest_product_being_purchased.property = "latest_product_being_purchased";
                latest_product_being_purchased.value = objResult.latest_product_being_purchased;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_product_being_purchased);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.latest_purchase_attempt_date.ToString()))
            {
                Contact latest_purchase_attempt_date = new Contact();
                latest_purchase_attempt_date.property = "latest_purchase_attempt_date";
                latest_purchase_attempt_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.latest_purchase_attempt_date);
                objProperties.properties = new List<Contact>();
                objList.Add(latest_purchase_attempt_date);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_purchase_attempt_payment_method))
            {
                Contact latest_purchase_attempt_payment_method = new Contact();
                latest_purchase_attempt_payment_method.property = "latest_purchase_attempt_payment_method";
                latest_purchase_attempt_payment_method.value = objResult.latest_purchase_attempt_payment_method;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_purchase_attempt_payment_method);
            }

            //string
            //Temp Commented
            if (!string.IsNullOrEmpty(objResult.latest_purchase_attempt_payment_mode))
            {
                Contact latest_purchase_attempt_payment_mode = new Contact();
                latest_purchase_attempt_payment_mode.property = "latest_purchase_attempt_payment_mode";
                latest_purchase_attempt_payment_mode.value = objResult.latest_purchase_attempt_payment_mode;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_purchase_attempt_payment_mode);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_purchase_attempt_payment_status))
            {
                Contact latest_purchase_attempt_payment_status = new Contact();
                latest_purchase_attempt_payment_status.property = "latest_purchase_attempt_payment_status";
                latest_purchase_attempt_payment_status.value = objResult.latest_purchase_attempt_payment_status;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_purchase_attempt_payment_status);
            }

            //string
            if (objResult.Latest_SIM_with_Topup_Amount_Being_Purchased > 0.0)
            {
                Contact Latest_SIM_with_Topup_Amount_Being_Purchased = new Contact();
                Latest_SIM_with_Topup_Amount_Being_Purchased.property = "Latest_SIM_with_Topup_Amount_Being_Purchased";
                Latest_SIM_with_Topup_Amount_Being_Purchased.value = objResult.Latest_SIM_with_Topup_Amount_Being_Purchased.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(Latest_SIM_with_Topup_Amount_Being_Purchased);
            }

            //string
            if (objResult.latest_topup_amount_being_purchased > 0.0)
            {
                Contact latest_topup_amount_being_purchased = new Contact();
                latest_topup_amount_being_purchased.property = "latest_topup_amount_being_purchased";
                latest_topup_amount_being_purchased.value = objResult.latest_topup_amount_being_purchased.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_amount_being_purchased);
            }
            //Datetime 
            if (!string.IsNullOrEmpty(objResult.latest_topup_date.ToString()))
            {
                Contact latest_topup_date = new Contact();
                latest_topup_date.property = "latest_topup_date";
                latest_topup_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.latest_topup_date);
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_date);
            }

            //string
            //Temp Commented
            if (!string.IsNullOrEmpty(objResult.latest_topup_method_used1))
            {
                Contact latest_topup_method_used1 = new Contact();
                latest_topup_method_used1.property = "latest_topup_method_used1";
                latest_topup_method_used1.value = objResult.latest_topup_method_used1;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_method_used1);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_topup_payment_method_used))
            {
                Contact latest_topup_payment_method_used = new Contact();
                latest_topup_payment_method_used.property = "latest_topup_payment_method_used";
                latest_topup_payment_method_used.value = objResult.latest_topup_payment_method_used;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_payment_method_used);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.latest_topup_payment_reference_number))
            {
                Contact latest_topup_payment_reference_number = new Contact();
                latest_topup_payment_reference_number.property = "latest_topup_payment_reference_number";
                latest_topup_payment_reference_number.value = objResult.latest_topup_payment_reference_number;
                objProperties.properties = new List<Contact>();
                objList.Add(latest_topup_payment_reference_number);
            }
            //string
            if (!string.IsNullOrEmpty(objResult.lifecyclestage))
            {
                Contact lifecyclestage = new Contact();
                lifecyclestage.property = "lifecyclestage";
                lifecyclestage.value = objResult.lifecyclestage;
                objProperties.properties = new List<Contact>();
                objList.Add(lifecyclestage);
            }
            //int 
            if (objResult.no_of_times_top_up_20_and_above != null)
            {
                Contact no_of_times_top_up_20_and_above = new Contact();
                no_of_times_top_up_20_and_above.property = "no_of_times_top_up_20_and_above";
                no_of_times_top_up_20_and_above.value = objResult.no_of_times_top_up_20_and_above.ToString().Trim();
                objList.Add(no_of_times_top_up_20_and_above);
            }
            //int 
            if (objResult.number_of_international_topups_made != null)
            {
                Contact number_of_international_topups_made = new Contact();
                number_of_international_topups_made.property = "number_of_international_topups_made";
                number_of_international_topups_made.value = objResult.number_of_international_topups_made.ToString().Trim();
                objList.Add(number_of_international_topups_made);
            }
            //int 
            if (objResult.number_of_referrals_made != null)
            {
                Contact number_of_referrals_made = new Contact();
                number_of_referrals_made.property = "number_of_referrals_made";
                number_of_referrals_made.value = objResult.number_of_referrals_made.ToString().Trim();
                objList.Add(number_of_referrals_made);
            }
            //int 
            if (objResult.number_of_topups_made != null)
            {
                Contact number_of_topups_made = new Contact();
                number_of_topups_made.property = "number_of_topups_made";
                number_of_topups_made.value = objResult.number_of_topups_made.ToString().Trim();
                objList.Add(number_of_topups_made);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.phone))
            {
                Contact phone = new Contact();
                phone.property = "phone";
                phone.value = objResult.phone;
                objProperties.properties = new List<Contact>();
                objList.Add(phone);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.portin_current_network_name))
            {
                Contact portin_current_network_name = new Contact();
                portin_current_network_name.property = "portin_current_network_name";
                portin_current_network_name.value = objResult.portin_current_network_name;
                objProperties.properties = new List<Contact>();
                objList.Add(portin_current_network_name);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.portin_request_completion_status))
            {
                Contact portin_request_completion_status = new Contact();
                portin_request_completion_status.property = "portin_request_completion_status";
                portin_request_completion_status.value = objResult.portin_request_completion_status;
                objProperties.properties = new List<Contact>();
                objList.Add(portin_request_completion_status);
            }


            //string
            if (!string.IsNullOrEmpty(objResult.portin_request_failure_reason))
            {
                Contact portin_request_failure_reason = new Contact();
                portin_request_failure_reason.property = "portin_request_failure_reason";
                portin_request_failure_reason.value = objResult.portin_request_failure_reason;
                objProperties.properties = new List<Contact>();
                objList.Add(portin_request_failure_reason);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.portout_network_name))
            {
                Contact portout_network_name = new Contact();
                portout_network_name.property = "portout_network_name";
                portout_network_name.value = objResult.portout_network_name;
                objProperties.properties = new List<Contact>();
                objList.Add(portout_network_name);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.reason_for_portout))
            {
                Contact reason_for_portout = new Contact();
                reason_for_portout.property = "reason_for_portout";
                reason_for_portout.value = objResult.reason_for_portout;
                objProperties.properties = new List<Contact>();
                objList.Add(reason_for_portout);
            }

            //string
            if (objResult.referee_subscriber_id > 0.0)
            {
                Contact referee_subscriber_id = new Contact();
                referee_subscriber_id.property = "referee_subscriber_id";
                referee_subscriber_id.value = objResult.referee_subscriber_id.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(referee_subscriber_id);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.referred_someone))
            {
                Contact referred_someone = new Contact();
                referred_someone.property = "referred_someone";
                referred_someone.value = objResult.referred_someone;
                objProperties.properties = new List<Contact>();
                objList.Add(referred_someone);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.referred_someone_first_time))
            {
                Contact referred_someone_first_time = new Contact();
                referred_someone_first_time.property = "referred_someone_first_time";
                referred_someone_first_time.value = objResult.referred_someone_first_time;
                objProperties.properties = new List<Contact>();
                objList.Add(referred_someone_first_time);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.registered_to_myaccount))
            {
                Contact registered_to_myaccount = new Contact();
                registered_to_myaccount.property = "registered_to_myaccount";
                registered_to_myaccount.value = objResult.registered_to_myaccount;
                objProperties.properties = new List<Contact>();
                objList.Add(registered_to_myaccount);
            }
            //double 
            if (objResult.revenue_from_topup != null)
            {
                Contact revenue_from_topup = new Contact();
                revenue_from_topup.property = "revenue_from_topup";
                revenue_from_topup.value = objResult.revenue_from_topup.ToString().Trim();
                objList.Add(revenue_from_topup);
            }

            //string
            if (objResult.roaming_Bundle_data_allowance > 0.0)
            {
                Contact roaming_Bundle_data_allowance = new Contact();
                roaming_Bundle_data_allowance.property = "roaming_Bundle_data_allowance";
                roaming_Bundle_data_allowance.value = objResult.roaming_Bundle_data_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_data_allowance);
            }

            //string
            if (objResult.roaming_Bundle_data_used_allowance_percentage > 0.0)
            {
                Contact roaming_Bundle_data_used_allowance_percentage = new Contact();
                roaming_Bundle_data_used_allowance_percentage.property = "roaming_Bundle_data_used_allowance_percentage";
                roaming_Bundle_data_used_allowance_percentage.value = objResult.roaming_Bundle_data_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_data_used_allowance_percentage);
            }

            //string
            if (objResult.roaming_Bundle_international_mins_total_allowance > 0.0)
            {
                Contact roaming_Bundle_international_mins_total_allowance = new Contact();
                roaming_Bundle_international_mins_total_allowance.property = "roaming_Bundle_international_mins_total_allowance";
                roaming_Bundle_international_mins_total_allowance.value = objResult.roaming_Bundle_international_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_international_mins_total_allowance);
            }

            //string
            if (objResult.roaming_Bundle_international_mins_used_allowance_percentage > 0.0)
            {
                Contact roaming_Bundle_international_mins_used_allowance_percentage = new Contact();
                roaming_Bundle_international_mins_used_allowance_percentage.property = "roaming_Bundle_international_mins_used_allowance_percentage";
                roaming_Bundle_international_mins_used_allowance_percentage.value = objResult.roaming_Bundle_international_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_international_mins_used_allowance_percentage);
            }

            //string
            if (objResult.roaming_Bundle_most_international_mins_used_country > 0.0)
            {
                Contact roaming_Bundle_most_international_mins_used_country = new Contact();
                roaming_Bundle_most_international_mins_used_country.property = "roaming_Bundle_most_international_mins_used_country";
                roaming_Bundle_most_international_mins_used_country.value = objResult.roaming_Bundle_most_international_mins_used_country.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_most_international_mins_used_country);
            }

            //string
            if (objResult.roaming_Bundle_most_international_mins_used_country_percentage > 0.0)
            {
                Contact roaming_Bundle_most_international_mins_used_country_percentage = new Contact();
                roaming_Bundle_most_international_mins_used_country_percentage.property = "roaming_Bundle_most_international_mins_used_country_percentage";
                roaming_Bundle_most_international_mins_used_country_percentage.value = objResult.roaming_Bundle_most_international_mins_used_country_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_most_international_mins_used_country_percentage);
            }

            //string
            if (objResult.roaming_Bundle_national_mins_total_allowance > 0.0)
            {
                Contact roaming_Bundle_national_mins_total_allowance = new Contact();
                roaming_Bundle_national_mins_total_allowance.property = "roaming_Bundle_national_mins_total_allowance";
                roaming_Bundle_national_mins_total_allowance.value = objResult.roaming_Bundle_national_mins_total_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_national_mins_total_allowance);
            }

            //string
            if (objResult.roaming_Bundle_national_mins_used_allowance_percentage > 0.0)
            {
                Contact roaming_Bundle_national_mins_used_allowance_percentage = new Contact();
                roaming_Bundle_national_mins_used_allowance_percentage.property = "roaming_Bundle_national_mins_used_allowance_percentage";
                roaming_Bundle_national_mins_used_allowance_percentage.value = objResult.roaming_Bundle_national_mins_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_national_mins_used_allowance_percentage);
            }

            //string
            if (objResult.roaming_Bundle_sms_allowance > 0.0)
            {
                Contact roaming_Bundle_sms_allowance = new Contact();
                roaming_Bundle_sms_allowance.property = "roaming_Bundle_sms_allowance";
                roaming_Bundle_sms_allowance.value = objResult.roaming_Bundle_sms_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_sms_allowance);
            }

            //string
            if (objResult.roaming_Bundle_sms_used_allowance_percentage > 0.0)
            {
                Contact roaming_Bundle_sms_used_allowance_percentage = new Contact();
                roaming_Bundle_sms_used_allowance_percentage.property = "roaming_Bundle_sms_used_allowance_percentage";
                roaming_Bundle_sms_used_allowance_percentage.value = objResult.roaming_Bundle_sms_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_sms_used_allowance_percentage);
            }

            //string
            if (objResult.roaming_Bundle_vectone_to_vectone_minutes_allowance > 0.0)
            {
                Contact roaming_Bundle_vectone_to_vectone_minutes_allowance = new Contact();
                roaming_Bundle_vectone_to_vectone_minutes_allowance.property = "roaming_Bundle_vectone_to_vectone_minutes_allowance";
                roaming_Bundle_vectone_to_vectone_minutes_allowance.value = objResult.roaming_Bundle_vectone_to_vectone_minutes_allowance.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_vectone_to_vectone_minutes_allowance);
            }

            //string
            if (objResult.roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage > 0.0)
            {
                Contact roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = new Contact();
                roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.property = "roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage";
                roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.value = objResult.roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage.ToString().Trim();
                objProperties.properties = new List<Contact>();
                objList.Add(roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.sim_country))
            {
                Contact sim_country = new Contact();
                sim_country.property = "sim_country";
                sim_country.value = objResult.sim_country;
                objProperties.properties = new List<Contact>();
                objList.Add(sim_country);
            }
            //datetime 
            if (!string.IsNullOrEmpty(objResult.sim_inserted_date.ToString()))
            {
                Contact sim_inserted_date = new Contact();
                sim_inserted_date.property = "sim_inserted_date";
                sim_inserted_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.sim_inserted_date);
                objProperties.properties = new List<Contact>();
                objList.Add(sim_inserted_date);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.sim_order_status))
            {
                Contact sim_order_status = new Contact();
                sim_order_status.property = "sim_order_status";
                sim_order_status.value = objResult.sim_order_status;
                objProperties.properties = new List<Contact>();
                objList.Add(sim_order_status);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.SIM_ordered_by))
            {
                Contact SIM_ordered_by = new Contact();
                SIM_ordered_by.property = "SIM_ordered_by";
                SIM_ordered_by.value = objResult.SIM_ordered_by;
                objProperties.properties = new List<Contact>();
                objList.Add(SIM_ordered_by);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.Smart_Rates_user))
            {
                Contact Smart_Rates_user = new Contact();
                Smart_Rates_user.property = "Smart_Rates_user";
                Smart_Rates_user.value = objResult.Smart_Rates_user;
                objProperties.properties = new List<Contact>();
                objList.Add(Smart_Rates_user);
            }
            //double 
            if (objResult.total_amount_of_international_topup_made != null)
            {
                Contact total_amount_of_international_topup_made = new Contact();
                total_amount_of_international_topup_made.property = "total_amount_of_international_topup_made";
                total_amount_of_international_topup_made.value = objResult.total_amount_of_international_topup_made.ToString().Trim();
                objList.Add(total_amount_of_international_topup_made);
            }
            //double 
            if (objResult.total_purchase_amount_of_the_customer != null)
            {
                Contact total_purchase_amount_of_the_customer = new Contact();
                total_purchase_amount_of_the_customer.property = "total_purchase_amount_of_the_customer";
                total_purchase_amount_of_the_customer.value = objResult.total_purchase_amount_of_the_customer.ToString().Trim();
                objList.Add(total_purchase_amount_of_the_customer);
            }

            //string
            if (!string.IsNullOrEmpty(objResult.zip))
            {
                Contact ZIP = new Contact();
                ZIP.property = "ZIP";
                ZIP.value = objResult.zip;
                objProperties.properties = new List<Contact>();
                objList.Add(ZIP);
            }
            if (objResult.brand_name != null)
            {
                Contact brand_name = new Contact();
                brand_name.property = "brand_name";
                brand_name.value = objResult.brand_name.ToString().Trim();
                objList.Add(brand_name);
            }
            try
            {
                if (objResult.credit_allowance_used != null)
                {
                    Contact credit_allowance_used = new Contact();
                    credit_allowance_used.property = "credit_allowance_used";
                    credit_allowance_used.value = objResult.credit_allowance_used.ToString().Trim();
                    objList.Add(credit_allowance_used);
                }
                if (!string.IsNullOrEmpty(objResult.nextbillingdate.ToString()))
                {
                    Contact nextbillingdate = new Contact();
                    nextbillingdate.property = "nextbillingdate";
                    nextbillingdate.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.nextbillingdate);
                    objList.Add(nextbillingdate);
                }
                if (!string.IsNullOrEmpty(objResult.landlinenumber))
                {
                    Contact landlinenumber = new Contact();
                    landlinenumber.property = "landlinenumber";
                    landlinenumber.value = objResult.landlinenumber.ToString().Trim();
                    objList.Add(landlinenumber);
                }
                if (!string.IsNullOrEmpty(objResult.existing_customer_my_vectone_charge_back))
                {
                    Contact existing_customer_my_vectone_charge_back = new Contact();
                    existing_customer_my_vectone_charge_back.property = "existing_customer_my_vectone_charge_back";
                    existing_customer_my_vectone_charge_back.value = objResult.existing_customer_my_vectone_charge_back.ToString().Trim();
                    objList.Add(existing_customer_my_vectone_charge_back);
                }
                if (!string.IsNullOrEmpty(objResult.requested_for_sim_swap))
                {
                    Contact requested_for_sim_swap = new Contact();
                    requested_for_sim_swap.property = "requested_for_sim_swap";
                    requested_for_sim_swap.value = objResult.requested_for_sim_swap.ToString().Trim();
                    objList.Add(requested_for_sim_swap);
                }
                if (!string.IsNullOrEmpty(objResult.sim_swap_status))
                {
                    Contact sim_swap_status = new Contact();
                    sim_swap_status.property = "sim_swap_status";
                    sim_swap_status.value = objResult.sim_swap_status.ToString().Trim();
                    objList.Add(sim_swap_status);
                }
                //simonly_excessusage
                if (!string.IsNullOrEmpty(objResult.simonly_excessusage))
                {
                    Contact simonly_excessusage = new Contact();
                    simonly_excessusage.property = "simonly_excessusage";
                    simonly_excessusage.value = objResult.simonly_excessusage.ToString().Trim();
                    objList.Add(simonly_excessusage);
                }
                //string
                if (!string.IsNullOrEmpty(objResult.portin_request))
                {
                    Contact portin_request = new Contact();
                    portin_request.property = "portin_request";
                    portin_request.value = objResult.portin_request;
                    objProperties.properties = new List<Contact>();
                    objList.Add(portin_request);
                }
                //string
                if (!string.IsNullOrEmpty(objResult.portin_status))
                {
                    Contact portin_status = new Contact();
                    portin_status.property = "portin_status";
                    portin_status.value = objResult.portin_status;
                    objProperties.properties = new List<Contact>();
                    objList.Add(portin_status);
                }
                if (!string.IsNullOrEmpty(objResult.last_topup_vou_method))
                {
                    Contact lastest_top_up_voucher_method = new Contact();
                    lastest_top_up_voucher_method.property = "lastest_top_up_voucher_method";
                    lastest_top_up_voucher_method.value = objResult.last_topup_vou_method;
                    objProperties.properties = new List<Contact>();
                    objList.Add(lastest_top_up_voucher_method);
                }

                if (!string.IsNullOrEmpty(objResult.latest_product_voucher_attempt_status))
                {
                    Contact latest_product_voucher_attempt_status = new Contact();
                    latest_product_voucher_attempt_status.property = "latest_product_voucher_attempt_status";
                    latest_product_voucher_attempt_status.value = objResult.latest_product_voucher_attempt_status;
                    objProperties.properties = new List<Contact>();
                    objList.Add(latest_product_voucher_attempt_status);
                }

                if (!string.IsNullOrEmpty(objResult.lat_product_vou_failed_reason))
                {
                    Contact latest_product_voucher_failed_reason = new Contact();
                    latest_product_voucher_failed_reason.property = "latest_product_voucher_failed_reason";
                    latest_product_voucher_failed_reason.value = objResult.lat_product_vou_failed_reason;
                    objProperties.properties = new List<Contact>();
                    objList.Add(latest_product_voucher_failed_reason);
                }
                ////types_of_bundle
                if (!string.IsNullOrEmpty(objResult.types_of_bundle))
                {
                    Contact types_of_bundle = new Contact();
                    types_of_bundle.property = "types_of_bundle";
                    types_of_bundle.value = objResult.types_of_bundle;
                    objProperties.properties = new List<Contact>();
                    objList.Add(types_of_bundle);
                }
      
                //sim_only_rollover_data_allowance
              if (objResult.sim_only_rollover_data_allowance > 0.0)
                {
                    Contact sim_only_rollover_data_allowance = new Contact();
                    sim_only_rollover_data_allowance.property = "sim_only_rollover_data_allowance";
                    sim_only_rollover_data_allowance.value = objResult.sim_only_rollover_data_allowance.ToString().Trim();
                    objProperties.properties = new List<Contact>();
                    objList.Add(sim_only_rollover_data_allowance);
                }
                //string
                if (!string.IsNullOrEmpty(objResult.credit_card_number))
                {
                    Contact credit_card_number = new Contact();
                    credit_card_number.property = "credit_card_number";
                    //credit_card_number.value = objResult.credit_card_number.ToString().Trim();
                    credit_card_number.value = "XXXX";
                    objProperties.properties = new List<Contact>();
                    objList.Add(credit_card_number);
                } 

                if (!string.IsNullOrEmpty(objResult.credit_card_expiry_date.ToString()))
                {
                    Contact credit_card_expiry_date = new Contact();
                    credit_card_expiry_date.property = "credit_card_expiry_date";
                    credit_card_expiry_date.value = objResult.credit_card_expiry_date;
                    objList.Add(credit_card_expiry_date);
                }
                if (!string.IsNullOrEmpty(objResult.credit_card_type))
                {
                    Contact credit_card_type = new Contact();
                    credit_card_type.property = "credit_card_type";
                    credit_card_type.value = objResult.credit_card_type;
                    objProperties.properties = new List<Contact>();
                    objList.Add(credit_card_type);
                }

                if (!string.IsNullOrEmpty(objResult.plan_change_date.ToString()))
                {
                    Contact plan_change_date = new Contact();
                    plan_change_date.property = "plan_change_date";
                    plan_change_date.value = UnixMillisecondDateTimeConverter.convertDateTimeToEpochvalue(objResult.plan_change_date);
                    objList.Add(plan_change_date);
                }
                if (!string.IsNullOrEmpty(objResult.plan_upgrade))
                {
                    Contact plan_upgrade = new Contact();
                    plan_upgrade.property = "plan_upgrade";
                    plan_upgrade.value = objResult.plan_upgrade;
                    objProperties.properties = new List<Contact>();
                    objList.Add(plan_upgrade);
                }
                if (!string.IsNullOrEmpty(objResult.plan_downgrade))
                {
                    Contact plan_downgrade = new Contact();
                    plan_downgrade.property = "plan_downgrade";
                    plan_downgrade.value = objResult.plan_downgrade;
                    objProperties.properties = new List<Contact>();
                    objList.Add(plan_downgrade);
                }

                //change_plan_requested_name = r.change_plan_requested_name 

            }
            catch (Exception ex)
            {


            }

            objProperties.properties = objList;

        }

        private static void CreateProperties(UrcrmusergetdetailsOutput objResult, out List<Contact> objList)
        {
            Properties objProperties = new Properties();
            objList = new List<Contact>();

            if (!string.IsNullOrEmpty(objResult.iccid))
            {
                Contact iccid = new Contact();
                iccid.property = "iccid";
                iccid.value = objResult.iccid;
                objProperties.properties = new List<Contact>();
                objList.Add(iccid);
            }
            if (!string.IsNullOrEmpty(objResult.firstupdate.ToString()))
            {
                Contact firstupdate = new Contact();
                firstupdate.property = "firstupdate";
                //  firstupdate.value = convertDateTimeToEpoch(objResult.firstupdate).ToString().Trim();
                firstupdate.value = (objResult.firstupdate).ToString().Trim();
                objList.Add(firstupdate);
            }
            if (!string.IsNullOrEmpty(objResult.lastupdate.ToString().Trim()))
            {
                Contact lastupdate = new Contact();
                lastupdate.property = "lastupdate";
                lastupdate.value = convertDateTimeToEpoch1(Convert.ToDateTime(objResult.lastupdate)).ToString().Trim();
                //lastupdate.value = (objResult.lastupdate).ToString().Trim();
                objList.Add(lastupdate);
            }
            if (!string.IsNullOrEmpty(objResult.mobileno))
            {
                Contact mobileno = new Contact();
                mobileno.property = "mobileno";
                mobileno.value = objResult.mobileno;
                mobileno.value = objResult.mobileno.ToString().Trim();
                objList.Add(mobileno);
            }
            //phone
            if (!string.IsNullOrEmpty(objResult.mobileno))
            {
                Contact phone = new Contact();
                phone.property = "phone";
                phone.value = objResult.mobileno;
                phone.value = objResult.mobileno.ToString().Trim();
                objList.Add(phone);
            }
            if (!string.IsNullOrEmpty(objResult.customer_type))
            {
                Contact customer_type = new Contact();
                customer_type.property = "customer_type";
                customer_type.value = objResult.customer_type.Trim();
                objList.Add(customer_type);
            }
            if (objResult.freesimid != null)
            {
                Contact freesimid = new Contact();
                freesimid.property = "freesimid";
                freesimid.value = objResult.freesimid.ToString().Trim();
                objList.Add(freesimid);
            }
            if (!string.IsNullOrEmpty(objResult.auto_topup))
            {
                Contact auto_topup = new Contact();
                auto_topup.property = "auto_topup";
                auto_topup.value = objResult.auto_topup.ToString().Trim();
                objList.Add(auto_topup);
            }
            if (objResult.subscriberid != null)
            {
                Contact subscriberid = new Contact();
                subscriberid.property = "subscriberid";
                subscriberid.value = objResult.subscriberid.ToString().Trim();
                objList.Add(subscriberid);
            }
            if (objResult.firstname != null)
            {
                Contact firstname = new Contact();
                firstname.property = "firstname";
                firstname.value = objResult.firstname.ToString().Trim();
                objList.Add(firstname);
            }
            if (objResult.lastname != null)
            {
                Contact lastname = new Contact();
                lastname.property = "lastname";
                lastname.value = objResult.lastname.ToString().Trim();
                objList.Add(lastname);
            }
            if (objResult.email != null && IsValidEmail(objResult.email))
            {
                Contact email = new Contact();
                email.property = "customer_email_ID";
                email.value = objResult.email.ToString().Trim();
                objList.Add(email);
            }
            //if (objResult.email != null && IsValidEmail(objResult.email))
            //{
            //    Contact email = new Contact();
            //    email.property = "customer_email_ID";
            //    email.value = objResult.email.ToString().Trim();
            //    objList.Add(email);
            //}

            if (objResult.houseno != null)
            {
                Contact houseno = new Contact();
                houseno.property = "houseno";
                houseno.value = objResult.houseno.ToString().Trim();
                objList.Add(houseno);
            }
            if (objResult.address1 != null)
            {
                Contact address1 = new Contact();
                address1.property = "address1";
                address1.value = objResult.address1.ToString().Trim();
                objList.Add(address1);
            }
            if (objResult.address2 != null)
            {
                Contact address2 = new Contact();
                address2.property = "address2";
                address2.value = objResult.address2.ToString().Trim();
                objList.Add(address2);
            }
            if (objResult.postcode != null)
            {
                Contact postcode = new Contact();
                postcode.property = "postcode";
                postcode.value = objResult.postcode.ToString().Trim();
                objList.Add(postcode);
            }
            //Temp commented
            //if (objResult.joined != null)
            //{
            //    Contact joined = new Contact();
            //    joined.property = "joined";
            //    joined.value = objResult.joined.ToString().Trim();
            //    objList.Add(joined);
            //}
            if (objResult.current_national_bundle_id != null)
            {
                Contact current_national_bundle_id = new Contact();
                current_national_bundle_id.property = "current_national_bundle_id";
                current_national_bundle_id.value = objResult.current_national_bundle_id.ToString().Trim();
                objList.Add(current_national_bundle_id);
            }
            if (objResult.current_national_bundle_name != null)
            {
                Contact current_national_bundle_name = new Contact();
                current_national_bundle_name.property = "current_national_bundle_name";
                current_national_bundle_name.value = objResult.current_national_bundle_name.ToString().Trim();
                objList.Add(current_national_bundle_name);
            }
            if (objResult.current_national_startdate != null)
            {
                Contact current_national_startdate = new Contact();
                current_national_startdate.property = "current_national_startdate";
                current_national_startdate.value = objResult.current_national_startdate.ToString().Trim();
                objList.Add(current_national_startdate);
            }
            if (objResult.current_national_enddate != null)
            {
                Contact current_national_enddate = new Contact();
                current_national_enddate.property = "current_national_enddate";
                current_national_enddate.value = objResult.current_national_enddate.ToString().Trim();
                objList.Add(current_national_enddate);
            }
            if (objResult.current_international_bundle_id != null)
            {
                Contact current_international_bundle_id = new Contact();
                current_international_bundle_id.property = "current_international_bundle_id";
                current_international_bundle_id.value = objResult.current_international_bundle_id.ToString().Trim();
                objList.Add(current_international_bundle_id);
            }
            if (objResult.current_international_bundle_name != null)
            {
                Contact current_international_bundle_name = new Contact();
                current_international_bundle_name.property = "current_international_bundle_name";
                current_international_bundle_name.value = objResult.current_international_bundle_name.ToString().Trim();
                objList.Add(current_international_bundle_name);
            }
            if (objResult.current_international_startdate != null)
            {
                Contact current_international_startdate = new Contact();
                current_international_startdate.property = "current_international_startdate";
                current_international_startdate.value = objResult.current_international_startdate.ToString().Trim();
                objList.Add(current_international_startdate);
            }
            if (objResult.current_international_enddate != null)
            {
                Contact current_international_enddate = new Contact();
                current_international_enddate.property = "current_international_enddate";
                current_international_enddate.value = objResult.current_international_enddate.ToString().Trim();
                objList.Add(current_international_enddate);
            }

            if (objResult.current_Data_bundle_id != null)
            {
                Contact current_Data_bundle_id = new Contact();
                current_Data_bundle_id.property = "current_Data_bundle_id";
                current_Data_bundle_id.value = objResult.current_Data_bundle_id.ToString();
                objList.Add(current_Data_bundle_id);
            }

            if (objResult.current_Data_bundle_name != null)
            {
                Contact current_Data_bundle_name = new Contact();
                current_Data_bundle_name.property = "current_Data_bundle_name";
                current_Data_bundle_name.value = objResult.current_Data_bundle_name.ToString().Trim();
                objList.Add(current_Data_bundle_name);
            }
            if (objResult.current_Data_startdate != null)
            {
                Contact current_Data_startdate = new Contact();
                current_Data_startdate.property = "current_Data_startdate";
                current_Data_startdate.value = objResult.current_Data_startdate.ToString().Trim();
                objList.Add(current_Data_startdate);
            }
            if (objResult.current_Data_enddate != null)
            {
                Contact current_Data_enddate = new Contact();
                current_Data_enddate.property = "current_Data_enddate";
                current_Data_enddate.value = objResult.current_Data_enddate.ToString().Trim();
                objList.Add(current_Data_enddate);
            }
            if (objResult.current_AIO_bundle_id != null)
            {
                Contact current_AIO_bundle_id = new Contact();
                current_AIO_bundle_id.property = "current_AIO_bundle_id";
                current_AIO_bundle_id.value = objResult.current_AIO_bundle_id.ToString().Trim();
                objList.Add(current_AIO_bundle_id);
            }
            if (objResult.current_AIO_bundle_name != null)
            {
                Contact current_AIO_bundle_name = new Contact();
                current_AIO_bundle_name.property = "current_AIO_bundle_name";
                current_AIO_bundle_name.value = objResult.current_AIO_bundle_name.ToString().Trim();
                objList.Add(current_AIO_bundle_name);
            }
            if (objResult.current_AIO_startdate != null)
            {
                Contact current_AIO_startdate = new Contact();
                current_AIO_startdate.property = "current_AIO_startdate";
                current_AIO_startdate.value = objResult.current_AIO_startdate.ToString().Trim();
                objList.Add(current_AIO_startdate);
            }
            if (objResult.current_AIO_enddate != null)
            {
                Contact current_AIO_enddate = new Contact();
                current_AIO_enddate.property = "current_AIO_enddate";
                current_AIO_enddate.value = objResult.current_AIO_enddate.ToString().Trim();
                objList.Add(current_AIO_enddate);
            }
            if (objResult.current_SIMONLY_bundle_id != null)
            {
                Contact current_SIMONLY_bundle_id = new Contact();
                current_SIMONLY_bundle_id.property = "current_SIMONLY_bundle_id";
                current_SIMONLY_bundle_id.value = objResult.current_SIMONLY_bundle_id.ToString().Trim();
                objList.Add(current_SIMONLY_bundle_id);
            }
            if (objResult.current_SIMONLY_bundle_name != null)
            {
                Contact current_SIMONLY_bundle_name = new Contact();
                current_SIMONLY_bundle_name.property = "current_SIMONLY_bundle_name";
                current_SIMONLY_bundle_name.value = objResult.current_SIMONLY_bundle_name.ToString().Trim();
                objList.Add(current_SIMONLY_bundle_name);
            }
            if (objResult.current_SIMONLY_startdate != null)
            {
                Contact current_SIMONLY_startdate = new Contact();
                current_SIMONLY_startdate.property = "current_SIMONLY_startdate";
                current_SIMONLY_startdate.value = objResult.current_SIMONLY_startdate.ToString().Trim();
                objList.Add(current_SIMONLY_startdate);
            }
            if (objResult.current_SIMONLY_enddate != null)
            {
                Contact current_SIMONLY_enddate = new Contact();
                current_SIMONLY_enddate.property = "current_SIMONLY_enddate";
                current_SIMONLY_enddate.value = objResult.current_SIMONLY_enddate.ToString().Trim();
                objList.Add(current_SIMONLY_enddate);
            }

            if (objResult.contact_id != null)
            {
                Contact contact_id = new Contact();
                contact_id.property = "contact_id";
                contact_id.value = objResult.contact_id.ToString().Trim();
                objList.Add(contact_id);
            }
            if (objResult.auto_renewal != null)
            {
                Contact auto_renewal = new Contact();
                auto_renewal.property = "auto_renewal";
                auto_renewal.value = objResult.auto_renewal.ToString().Trim();
                objList.Add(auto_renewal);
            }
            if (objResult.Smart_Rates_user != null)
            {
                Contact Smart_Rates_user = new Contact();
                Smart_Rates_user.property = "Smart_Rates_user";
                Smart_Rates_user.value = objResult.Smart_Rates_user.ToString().Trim();
                objList.Add(Smart_Rates_user);
            }
            if (objResult.latest_topup_method_used != null)
            {
                Contact latest_topup_method_used = new Contact();
                latest_topup_method_used.property = "latest_topup_method_used";
                latest_topup_method_used.value = objResult.latest_topup_method_used.ToString().Trim();
                objList.Add(latest_topup_method_used);
            }
            if (objResult.latest_topup_amount != null)
            {
                Contact latest_topup_amount = new Contact();
                latest_topup_amount.property = "latest_topup_amount";
                latest_topup_amount.value = objResult.latest_topup_amount.ToString().Trim();
                objList.Add(latest_topup_amount);
            }
            if (objResult.Country_code != null)
            {
                Contact Country_code = new Contact();
                Country_code.property = "Country_code";
                Country_code.value = objResult.Country_code.ToString().Trim();
                objList.Add(Country_code);
            }

            //Current bundle 2 data allowance
            if (objResult.current_bundle_2_data_allowance != null)
            {
                Contact current_bundle_2_data_allowance = new Contact();
                current_bundle_2_data_allowance.property = "Current bundle 2 data allowance";
                current_bundle_2_data_allowance.value = objResult.current_bundle_2_data_allowance.ToString();
                objList.Add(current_bundle_2_data_allowance);
            }


            objProperties.properties = objList;
        }

        public class UrcrmusergetdetailsInput
        {
            public string fromdate { get; set; }
            public string todate { get; set; }
        }
        public class SyncCustomersInput
        {
            public int contact_id { get; set; }
            public string mobileno { get; set; }
            public int processtype { get; set; }
            public string iccid { get; set; }
        }

        public class UrcrmusergetdetailsOutput12
        {
            public string iccid { get; set; }
            public DateTime? firstupdate { get; set; }
            public DateTime? lastupdate { get; set; }
            public string mobileno { get; set; }
            public string customer_type { get; set; }
            public int? freesimid { get; set; }
            public string auto_topup { get; set; }
            public int? subscriberid { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string houseno { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string postcode { get; set; }
            public string joined { get; set; }
            public int? current_national_bundle_id { get; set; }
            public string current_national_bundle_name { get; set; }
            public DateTime? current_national_startdate { get; set; }
            public DateTime? current_national_enddate { get; set; }
            public int? current_international_bundle_id { get; set; }
            public string current_international_bundle_name { get; set; }
            public DateTime? current_international_startdate { get; set; }
            public DateTime? current_international_enddate { get; set; }
            public int? current_Data_bundle_id { get; set; }
            public string current_Data_bundle_name { get; set; }
            public DateTime? current_Data_startdate { get; set; }
            public DateTime? current_Data_enddate { get; set; }
            public int? current_AIO_bundle_id { get; set; }
            public string current_AIO_bundle_name { get; set; }
            public DateTime? current_AIO_startdate { get; set; }
            public DateTime? current_AIO_enddate { get; set; }
            public int? current_SIMONLY_bundle_id { get; set; }
            public string current_SIMONLY_bundle_name { get; set; }
            public DateTime? current_SIMONLY_startdate { get; set; }
            public DateTime? current_SIMONLY_enddate { get; set; }

            public int? contact_id { get; set; }
            public string auto_renewal { get; set; }
            public string Smart_Rates_user { get; set; }
            public string latest_topup_method_used { get; set; }
            public double? latest_topup_amount { get; set; }
            public string Country_code { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public int current_bundle_2_data_allowance { get; set; }
        }
        public class UrcrmusergetdetailsOutput2
        {
            public string address1 { get; set; }
            public string address2 { get; set; }
            public double? AIO_Bundle_data_allowance { get; set; }
            public double? AIO_Bundle_data_used_allowance_percentage { get; set; }
            public double? AIO_Bundle_international_mins_total_allowance { get; set; }
            public double? AIO_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? AIO_Bundle_most_international_mins_used_country_percentage { get; set; }
            public double? AIO_Bundle_national_mins_total_allowance { get; set; }
            public double? AIO_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? AIO_Bundle_sms_allowance { get; set; }
            public double? AIO_Bundle_sms_used_allowance_percentage { get; set; }
            public double? AIO_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string alternate_email_id { get; set; }
            //public double? arpu { get; set; }//Need to check with DB Team           
            public string auto_renewal { get; set; }
            public string auto_renewal_disable_by { get; set; }


            public DateTime? auto_renewal_disable_date { get; set; }
            public string auto_renewal_enable_by { get; set; }
            public DateTime? auto_renewal_enable_date { get; set; }
            public string auto_topup { get; set; }
            public string auto_topup_disable_by { get; set; }
            public DateTime? auto_topup_disable_date { get; set; }
            public string auto_topup_enable_by { get; set; }
            public DateTime? auto_topup_enable_date { get; set; }
            public int? avg_customer_wait_time_between_purchases { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string country_code { get; set; }
            public int? current_addon_Data_bundle_id { get; set; }
            public string current_addon_Data_bundle_name { get; set; }
            public DateTime? current_addon_Data_enddate { get; set; }
            public DateTime? current_addon_Data_startdate { get; set; }
            public int? current_addon_international_bundle_id { get; set; }
            public string current_addon_international_bundle_name { get; set; }
            public DateTime? current_addon_international_enddate { get; set; }
            public DateTime? current_addon_international_startdate { get; set; }
            public int? current_addon_national_bundle_id { get; set; }
            public string current_addon_national_bundle_name { get; set; }
            public DateTime? current_addon_national_enddate { get; set; }
            public DateTime? current_addon_national_startdate { get; set; }
            public int? current_AIO_bundle_id { get; set; }
            public double? current_AIO_bundle_most_international_mins_used_country { get; set; }
            public string current_AIO_bundle_name { get; set; }
            public DateTime? current_AIO_bundle_renewal_date { get; set; }
            public DateTime? current_AIO_enddate { get; set; }
            public DateTime? current_AIO_startdate { get; set; }
            public double? current_balance { get; set; }
            public int? current_Data_bundle_id { get; set; }
            public string current_Data_bundle_name { get; set; }
            public DateTime? current_data_bundle_renewal_date { get; set; }
            public DateTime? current_Data_enddate { get; set; }
            public DateTime? current_data_roaming_addon_enddate { get; set; }
            public int? current_data_roaming_addon_id { get; set; }
            public string current_data_roaming_addon_name { get; set; }
            public DateTime? current_data_roaming_addon_startdate { get; set; }
            public DateTime? current_Data_startdate { get; set; }
            public int? current_international_bundle_id { get; set; }
            public double? current_international_bundle_most_international_mins_used_country { get; set; }
            public string current_international_bundle_name { get; set; }
            public DateTime? current_International_bundle_renewal_date { get; set; }
            public DateTime? current_international_enddate { get; set; }
            public DateTime? current_international_startdate { get; set; }
            public int? current_national_bundle_id { get; set; }
            public double? current_national_bundle_most_international_mins_used_country { get; set; }
            public string current_national_bundle_name { get; set; }
            public DateTime? current_national_bundle_renewal_date { get; set; }
            public DateTime? current_national_enddate { get; set; }
            public DateTime? current_national_startdate { get; set; }
            public DateTime? current_roaming_bundle_enddate { get; set; }
            public int? current_roaming_bundle_id { get; set; }
            public string current_roaming_bundle_name { get; set; }
            public DateTime? current_roaming_bundle_renewal_date { get; set; }
            public DateTime? current_roaming_bundle_startdate { get; set; }
            public int? current_SIMONLY_bundle_id { get; set; }
            public double? current_SIMONly_bundle_most_international_mins_used_country { get; set; }
            public double? current_SIMONly_bundle_most_international_mins_used_country_per { get; set; }
            public string current_SIMONLY_bundle_name { get; set; }
            public DateTime? current_SIMONLY_bundle_renewal_date { get; set; }
            public DateTime? current_SIMONLY_enddate { get; set; }
            public DateTime? current_SIMONLY_startdate { get; set; }
            public string customer_type { get; set; }
            public double? Data_addon_Bundle_data_allowance { get; set; }
            public double? Data_addon_Bundle_data_used_allowance_percentage { get; set; }
            //public double? Data_addon_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? Data_Bundle_data_allowance { get; set; }
            public double? Data_Bundle_data_used_allowance_percentage { get; set; }
            public double? Data_Bundle_international_mins_total_allowance { get; set; }
            public double? Data_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? Data_Bundle_most_international_mins_used_country { get; set; }
            public double? Data_Bundle_most_international_mins_used_country_percentage { get; set; }
            public double? Data_Bundle_national_mins_total_allowance { get; set; }
            public double? Data_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? Data_Bundle_sms_allowance { get; set; }
            public double? Data_Bundle_sms_used_allowance_percentage { get; set; }
            public double? Data_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public double? data_roaming_addon_data_total_allowance { get; set; }
            public double? data_roaming_addon_data_used_allowance_percentage { get; set; }
            public DateTime? date_of_birth { get; set; }
            public string email { get; set; }
            //Temp commented
            public string email_verification_status { get; set; }

            public string first_bundle_name { get; set; }
            public double? first_bundle_purchase_amount { get; set; }
            public DateTime? first_bundle_purchase_date { get; set; }
            public string first_bundle_purchase_method { get; set; }
            public string first_bundle_status { get; set; }
            public DateTime? first_referral_date { get; set; }
            public string first_time_bundle_purchase_payment_method_used { get; set; }
            public string first_time_purchased_bundle_category { get; set; }
            public DateTime? first_time_topup_date { get; set; }
            public string first_time_topup_payment_method_used { get; set; }
            public double? first_topup_amount { get; set; }

            //Temp commented
            public string first_topup_method_used { get; set; }

            public string first_topup_status { get; set; }
            public string firstname { get; set; }
            public double? free_credit { get; set; }//Need to check with DB Team
            public int? freesimid { get; set; }
            public string frequent_topup_method { get; set; }
            public string Has_active_AIO_bundle { get; set; }
            public string Has_active_data_addon { get; set; }
            public string Has_active_data_bundle { get; set; }
            public string Has_active_data_roaming_addon { get; set; }
            public string Has_active_international_addon { get; set; }
            public string Has_active_International_bundle { get; set; }
            public string Has_active_national_addon { get; set; }
            public string Has_active_national_bundle { get; set; }
            public string Has_active_roaming_bundle { get; set; }
            public string Has_active_SIMONLY_bundle { get; set; }
            public string has_llom { get; set; }
            public string has_vectone_extra { get; set; }
            public string houseno { get; set; }
            //Temp commented
            public string hs_analytics_source { get; set; }

            public string hs_language { get; set; }
            public string hs_lead_status { get; set; }
            public string hs_object_id { get; set; }
            public string iccid { get; set; }
            public double? International_addon_Bundle_international_mins_total_allowance { get; set; }
            public double? International_addon_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? International_Bundle_data_allowance { get; set; }
            public double? International_Bundle_data_used_allowance_percentage { get; set; }
            public double? International_Bundle_international_mins_total_allowance { get; set; }
            public double? International_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? International_Bundle_most_international_mins_used_country_percentage { get; set; }
            public double? International_Bundle_national_mins_total_allowance { get; set; }
            public double? International_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? International_Bundle_sms_allowance { get; set; }
            public double? International_Bundle_sms_used_allowance_percentage { get; set; }
            public double? International_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string international_topup_country_name { get; set; }
            public string international_topup_network_name { get; set; }
            public DateTime? last_location_update_date { get; set; }
            public DateTime? last_referral_date { get; set; }
            public DateTime? lastcalldate { get; set; }
            public DateTime? lastgprsdate { get; set; }
            public string lastname { get; set; }
            public DateTime? lastsmsdate { get; set; }
            public string latest_addon_being_purchased { get; set; }
            public string latest_attempt_payment_reference_number { get; set; }
            public DateTime? latest_auto_topup_attempt_date { get; set; }
            public string latest_auto_topup_attempt_failure_reason { get; set; }
            public string latest_auto_topup_attempt_status { get; set; }
            public int? latest_bundle_id_being_purchased { get; set; }
            public string latest_bundle_name_being_purchased { get; set; }
            public string latest_bundle_payment_method { get; set; }
            public string latest_bundle_payment_mode { get; set; }
            public string latest_bundle_payment_reference_number { get; set; }
            public DateTime? latest_bundle_purchase_date { get; set; }
            public DateTime? latest_bundle_renewal_attempt_date { get; set; }
            public string latest_bundle_renewal_attempt_failure_reason { get; set; }
            public string latest_bundle_renewal_attempt_status { get; set; }
            public string latest_buying_stage { get; set; }
            //public int? latest_international_topup_amount { get; set; }
            public double? latest_international_topup_amount { get; set; }
            public string latest_international_topup_request_completion_status { get; set; }
            public string latest_international_topup_request_failure_reason { get; set; }
            public string latest_payment_attempt_failure_reason { get; set; }
            public string Latest_Preloaded_SIM_Amount_Being_Purchased { get; set; }
            public string latest_product_being_purchased { get; set; }
            public DateTime? latest_purchase_attempt_date { get; set; }
            public string latest_purchase_attempt_payment_method { get; set; }
            //Temp commented
            public string latest_purchase_attempt_payment_mode { get; set; }
            public string latest_purchase_attempt_payment_status { get; set; }
            public string latest_sim_only_with_bundle_name_being_purchased { get; set; }
            public string latest_sim_with_bundle_name_being_purchased { get; set; }
            //public string Latest_SIM_with_Topup_Amount_Being_Purchased { get; set; }
            public double? Latest_SIM_with_Topup_Amount_Being_Purchased { get; set; }

            public double? latest_topup_amount { get; set; }
            public double? latest_topup_amount_being_purchased { get; set; }
            public DateTime? latest_topup_date { get; set; }
            //Temp Commented
            public string latest_topup_method_used1 { get; set; }
            public string latest_topup_payment_method_used { get; set; }
            public string latest_topup_payment_reference_number { get; set; }
            public string latest_topup_type { get; set; }
            public string lifecyclestage { get; set; }
            public string mobilephone { get; set; }
            public string most_subscribed_country_specific_bundle_name { get; set; }
            public double? National_addon_Bundle_national_mins_total_allowance { get; set; }
            public double? National_addon_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? National_Bundle_data_allowance { get; set; }
            public double? National_Bundle_data_used_allowance_percentage { get; set; }
            public double? National_Bundle_international_mins_total_allowance { get; set; }
            public double? National_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? National_Bundle_most_international_mins_used_country_percentage { get; set; }
            public double? National_Bundle_national_mins_total_allowance { get; set; }
            public double? National_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? National_Bundle_sms_allowance { get; set; }
            public double? National_Bundle_sms_used_allowance_percentage { get; set; }
            public double? National_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public int? no_of_times_top_up_20_and_above { get; set; }
            public int? number_of_active_addons { get; set; }
            public int? number_of_active_bundles { get; set; }
            public int? number_of_addons_purchased { get; set; }
            public int? number_of_bundles_purchased { get; set; }
            public int? number_of_international_topups_made { get; set; }
            public int? number_of_referrals_made { get; set; }
            public int? number_of_topups_made { get; set; }
            public string phone { get; set; }
            public string portin_current_network_name { get; set; }
            public string portin_request_completion_status { get; set; }

            public string portin_request { get; set; }
            public string portin_status { get; set; }

            public string portin_request_failure_reason { get; set; }
            public string portout_network_name { get; set; }
            public string previously_subscribed_for_country_specific_bundles_ { get; set; }
            public string reason_for_portout { get; set; }
            public int? referee_subscriber_id { get; set; }
            public string referred_someone { get; set; }
            public string referred_someone_first_time { get; set; }
            public string registered_to_myaccount { get; set; }
            public double? revenue_from_bundle { get; set; }
            public double? revenue_from_topup { get; set; }
            public double? roaming_Bundle_data_allowance { get; set; }
            public double? roaming_Bundle_data_used_allowance_percentage { get; set; }
            public double? roaming_Bundle_international_mins_total_allowance { get; set; }
            public double? roaming_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? roaming_Bundle_most_international_mins_used_country { get; set; }
            public double? roaming_Bundle_most_international_mins_used_country_percentage { get; set; }
            public double? roaming_Bundle_national_mins_total_allowance { get; set; }
            public double? roaming_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? roaming_Bundle_sms_allowance { get; set; }
            public double? roaming_Bundle_sms_used_allowance_percentage { get; set; }
            public double? roaming_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string roaming_status { get; set; }
            public string sim_country { get; set; }
            public DateTime? sim_dispatch_date { get; set; }
            public string sim_inserted { get; set; }
            public DateTime? sim_inserted_date { get; set; }
            public string sim_order_status { get; set; }
            public string SIM_ordered_by { get; set; }
            public string sim_type { get; set; }
            public double? simonly_Bundle_data_allowance { get; set; }
            public double? simonly_Bundle_data_used_allowance_percentage { get; set; }
            public double? simonly_Bundle_international_mins_total_allowance { get; set; }
            public double? simonly_Bundle_international_mins_used_allowance_percentage { get; set; }
            public double? simonly_Bundle_national_mins_total_allowance { get; set; }
            public double? simonly_Bundle_national_mins_used_allowance_percentage { get; set; }
            public double? simonly_Bundle_sms_allowance { get; set; }
            public double? simonly_Bundle_sms_used_allowance_percentage { get; set; }
            public double? simonly_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public double? simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string Smart_Rates_user { get; set; }
            public string state { get; set; }
            public int? subscriberid { get; set; }
            public double? total_amount_of_international_topup_made { get; set; }
            public double? total_purchase_amount_of_the_customer { get; set; }
            public string zip { get; set; }
            public string brand_name { get; set; }
            public string credit_allowance_used { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public DateTime? nextbillingdate { get; set; }
            public string landlinenumber { get; set; }
            public string existing_customer_my_vectone_charge_back { get; set; }
            public string requested_for_sim_swap { get; set; }
            public string sim_swap_status { get; set; }
            public string simonly_excessusage { get; set; }
            public string last_topup_vou_method { get; set; }
            public string lat_product_vou_failed_reason { get; set; }
            public string latest_product_voucher_attempt_status { get; set; }
            public string types_of_bundle { get; set; }

            public double? sim_only_rollover_data_allowance { get; set; }
            public string credit_card_number { get; set; }
            public string credit_card_expiry_date { get; set; }
            public string credit_card_type { get; set; }

            public string plan_upgrade { get; set; }
            public string plan_downgrade { get; set; }
            public string change_plan_requested_name { get; set; }
            public DateTime? plan_change_date { get; set; }
        }
        public class UrcrmusergetdetailsOutput
        {
            public string iccid { get; set; }
            public DateTime? firstupdate { get; set; }
            public DateTime? lastupdate { get; set; }
            public string mobileno { get; set; }
            public string customer_type { get; set; }
            public int? freesimid { get; set; }
            public string auto_topup { get; set; }
            public int? subscriberid { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string houseno { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string postcode { get; set; }

            //Temp commented
            // public string joined { get; set; }


            public int? current_national_bundle_id { get; set; }
            public string current_national_bundle_name { get; set; }
            public DateTime? current_national_startdate { get; set; }
            public DateTime? current_national_enddate { get; set; }
            public int? current_international_bundle_id { get; set; }
            public string current_international_bundle_name { get; set; }
            public DateTime? current_international_startdate { get; set; }
            public DateTime? current_international_enddate { get; set; }
            public int? current_Data_bundle_id { get; set; }
            public string current_Data_bundle_name { get; set; }
            public DateTime? current_Data_startdate { get; set; }
            public DateTime? current_Data_enddate { get; set; }
            public int? current_AIO_bundle_id { get; set; }
            public string current_AIO_bundle_name { get; set; }
            public DateTime? current_AIO_startdate { get; set; }
            public DateTime? current_AIO_enddate { get; set; }
            public int? current_SIMONLY_bundle_id { get; set; }
            public string current_SIMONLY_bundle_name { get; set; }
            public DateTime? current_SIMONLY_startdate { get; set; }
            public DateTime? current_SIMONLY_enddate { get; set; }

            public int? contact_id { get; set; }
            public string auto_renewal { get; set; }
            public string Smart_Rates_user { get; set; }
            public string latest_topup_method_used { get; set; }
            public double? latest_topup_amount { get; set; }
            public string Country_code { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public int current_bundle_2_data_allowance { get; set; }
        }
        public class UrcrmusergetdetailsContactOutput
        {
            public string ContactID { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class UrcrmusergetdetailsOutput1
        {
            //public string iccid { get; set; }
            //public DateTime? firstupdate { get; set; }
            //public DateTime? lastupdate { get; set; }
            //public string mobileno { get; set; }
            //public string customer_type { get; set; }
            //public int? freesimid { get; set; }
            //public string auto_topup { get; set; }
            //public int? subscriberid { get; set; }
            //public string firstname { get; set; }
            //public string lastname { get; set; }
            //public string email { get; set; }
            //public string houseno { get; set; }
            //public string address1 { get; set; }
            //public string address2 { get; set; }
            //public string postcode { get; set; }
            //public string joined { get; set; }
            //public int? current_national_bundle_id { get; set; }
            //public string current_national_bundle_name { get; set; }
            //public DateTime? current_national_startdate { get; set; }
            //public DateTime? current_national_enddate { get; set; }
            //public int? current_international_bundle_id { get; set; }
            //public string current_international_bundle_name { get; set; }
            //public DateTime? current_international_startdate { get; set; }
            //public DateTime? current_international_enddate { get; set; }
            //public int? current_Data_bundle_id { get; set; }
            //public string current_Data_bundle_name { get; set; }
            //public DateTime? current_Data_startdate { get; set; }
            //public DateTime? current_Data_enddate { get; set; }
            //public int? current_AIO_bundle_id { get; set; }
            //public string current_AIO_bundle_name { get; set; }
            //public DateTime? current_AIO_startdate { get; set; }
            //public DateTime? current_AIO_enddate { get; set; }
            //public int? current_SIMONLY_bundle_id { get; set; }
            //public string current_SIMONLY_bundle_name { get; set; }
            //public DateTime? current_SIMONLY_startdate { get; set; }
            //public DateTime? current_SIMONLY_enddate { get; set; }

            //public int? contact_id { get; set; }
            //public string auto_renewal { get; set; }
            //public string Smart_Rates_user { get; set; }
            //public string latest_topup_method_used { get; set; }
            //public double? latest_topup_amount { get; set; }
            //public string Country_code { get; set; }
            //public int? errcode { get; set; }
            //public string errmsg { get; set; }
            //public int current_bundle_2_data_allowance { get; set; }


            public string mobileno { get; set; }
            public int? auto_topup { get; set; }
            public string auto_topup_disable_by { get; set; }
            public DateTime? auto_topup_disable_date { get; set; }
            public string auto_topup_enable_by { get; set; }
            public DateTime? auto_topup_enable_date { get; set; }
            public int? no_of_topup { get; set; }
            public double? first_topup_amount { get; set; }
            public string first_topup_method_used { get; set; }
            public DateTime? first_time_topup_date { get; set; }
            public DateTime? latest_auto_topup_attempt_date { get; set; }
            public DateTime? latest_auto_topup_attempt_failure_reason { get; set; }
            public string latest_auto_topup_attempt_status { get; set; }
            public int? status { get; set; }

            public DateTime? insert_date { get; set; }
            public DateTime? last_topup_date { get; set; }
            public double? latest_topup_amount { get; set; }
            public string latest_topup_type { get; set; }
            public string last_topup_vou_method { get; set; }
            public string lat_topup_method_used { get; set; }
            public string last_topup_payment_ref { get; set; }
            public double? total_revenue { get; set; }
            public string int_topup_network_name { get; set; }
            public double? lat_int_topup_amount { get; set; }
            public string lat_int_topup_comp_status { get; set; }
            public int? no_int_topup_made { get; set; }
            public double? int_total_topup_amount { get; set; }
            public string int_topup_failure_reason { get; set; }
            public string iccid { get; set; }
            public DateTime? sim_login_date { get; set; }
            public DateTime? sim_dispatch_date { get; set; }
            public DateTime? sim_last_login_date { get; set; }
            public string sim_inserted { get; set; }
            public string sim_type { get; set; }
            public int? freesimid { get; set; }
            public int? subscriberid { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string houseno { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string postcode { get; set; }
            public DateTime? dob { get; set; }
            public int? hubspot_contact_id { get; set; }
            public string mobilephone { get; set; }
            public string ordersim_url { get; set; }
            public string number_of_bundles_purchased { get; set; }
            public string number_of_active_bundles { get; set; }
            public string first_bundle_name { get; set; }
            public string first_time_purchased_bundle_category { get; set; }
            public string first_bundle_purchase_amount { get; set; }
            public DateTime? first_bundle_purchase_date { get; set; }
            public string first_bundle_purchase_method { get; set; }
            public string first_time_bundle_purchase_payment_method_used { get; set; }
            public string first_bundle_status { get; set; }
            public string latest_bundle_renewal_attempt_date { get; set; }
            public string latest_bundle_renewal_attempt_failure_reason { get; set; }
            public string latest_bundle_renewal_attempt_status { get; set; }
            public string latest_bundle_name_being_purchased { get; set; }
            public string National_addon_Bundle_national_mins_total_allowance { get; set; }
            public string National_Bundle_data_allowance { get; set; }
            public string National_Bundle_data_used_allowance_percentage { get; set; }
            public string National_Bundle_international_mins_total_allowance { get; set; }
            public string National_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string National_Bundle_national_mins_total_allowance { get; set; }
            public string National_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string National_Bundle_sms_allowance { get; set; }
            public string National_Bundle_sms_used_allowance_percentage { get; set; }
            public string National_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public string AIO_Bundle_data_allowance { get; set; }
            public string AIO_Bundle_data_used_allowance_percentage { get; set; }
            public string AIO_Bundle_international_mins_total_allowance { get; set; }
            public string AIO_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string AIO_Bundle_national_mins_total_allowance { get; set; }
            public string AIO_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string AIO_Bundle_sms_allowance { get; set; }
            public string AIO_Bundle_sms_used_allowance_percentage { get; set; }
            public string AIO_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public string AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string current_addon_Data_bundle_id { get; set; }
            public string current_addon_Data_bundle_name { get; set; }
            public string current_addon_international_bundle_id { get; set; }
            public string current_addon_international_bundle_name { get; set; }
            public string current_addon_national_bundle_id { get; set; }
            public string current_addon_national_bundle_name { get; set; }
            public string current_AIO_bundle_id { get; set; }
            public string current_AIO_bundle_name { get; set; }
            public DateTime? current_AIO_bundle_renewal_date { get; set; }
            public string current_Data_bundle_id { get; set; }
            public string current_Data_bundle_name { get; set; }
            public string current_data_bundle_renewal_date { get; set; }
            public string current_international_bundle_id { get; set; }
            public string current_international_bundle_name { get; set; }
            public string current_International_bundle_renewal_date { get; set; }
            public string current_national_bundle_id { get; set; }
            public string current_national_bundle_name { get; set; }
            public DateTime? current_national_bundle_renewal_date { get; set; }
            public string current_SIMONLY_bundle_id { get; set; }
            public string current_SIMONLY_bundle_name { get; set; }
            public string Data_addon_Bundle_data_allowance { get; set; }
            public string Data_addon_Bundle_data_used_allowance_percentage { get; set; }
            public string Data_addon_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string Data_Bundle_data_allowance { get; set; }
            public string Data_Bundle_data_used_allowance_percentage { get; set; }
            public string Data_Bundle_international_mins_total_allowance { get; set; }
            public string Data_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string Data_Bundle_national_mins_total_allowance { get; set; }
            public string Data_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string Data_Bundle_sms_allowance { get; set; }
            public string Data_Bundle_sms_used_allowance_percentage { get; set; }
            public string Data_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public string Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string International_addon_Bundle_international_mins_total_allowance { get; set; }
            public string International_addon_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string International_Bundle_data_allowance { get; set; }
            public string International_Bundle_data_used_allowance_percentage { get; set; }
            public string International_Bundle_international_mins_total_allowance { get; set; }
            public string International_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string International_Bundle_national_mins_total_allowance { get; set; }
            public string International_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string International_Bundle_sms_allowance { get; set; }
            public string International_Bundle_sms_used_allowance_percentage { get; set; }
            public string International_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public string International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string latest_sim_with_bundle_name_being_purchased { get; set; }
            public string latest_sim_only_with_bundle_name_being_purchased { get; set; }
            public string National_addon_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string revenue_from_bundle { get; set; }
            public string simonly_Bundle_data_allowance { get; set; }
            public string simonly_Bundle_data_used_allowance_percentage { get; set; }
            public string simonly_Bundle_international_mins_total_allowance { get; set; }
            public string simonly_Bundle_international_mins_used_allowance_percentage { get; set; }
            public string simonly_Bundle_national_mins_total_allowance { get; set; }
            public string simonly_Bundle_national_mins_used_allowance_percentage { get; set; }
            public string simonly_Bundle_sms_allowance { get; set; }
            public string simonly_Bundle_sms_used_allowance_percentage { get; set; }
            public string simonly_Bundle_vectone_to_vectone_minutes_allowance { get; set; }
            public string simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string current_national_bundle_most_international_mins_used_country { get; set; }
            public string current_international_bundle_most_international_mins_used_country { get; set; }
            public string current_AIO_bundle_most_international_mins_used_country { get; set; }
            public string current_SIMONly_bundle_most_international_mins_used_country { get; set; }
            public string National_Bundle_most_international_mins_used_country_percentage { get; set; }
            public string International_Bundle_most_international_mins_used_country_percentage { get; set; }
            public string AIO_Bundle_most_international_mins_used_country_percentage { get; set; }
            public string Data_Bundle_most_international_mins_used_country { get; set; }
            public string Data_Bundle_most_international_mins_used_country_percentage { get; set; }
            public string current_SIMONly_bundle_most_international_mins_used_country_per { get; set; }
            public string previously_subscribed_for_country_specific_bundles_ { get; set; }
            public string most_subscribed_country_specific_bundle_name { get; set; }
            public string Has_active_national_bundle { get; set; }
            public string Has_active_AIO_bundle { get; set; }
            public string Has_active_International_bundle { get; set; }
            public string Has_active_data_bundle { get; set; }
            public string Has_active_roaming_bundle { get; set; }
            public string current_roam_bun_name { get; set; }
            public string current_roam_bun_id { get; set; }
            public string current_roam_bun_startdate { get; set; }
            public string current_roam_bun_enddate { get; set; }
            public string current_roam_bun_renewal_date { get; set; }
            public string roam_bun_data_allowance { get; set; }
            public string roam_bun_data_used_allowance_percentage { get; set; }
            public string roam_bun_national_mins_total_allowance { get; set; }
            public string roam_bun_national_mins_used_allowance_percentage { get; set; }
            public string roam_bun_international_mins_total_allowance { get; set; }
            public string roam_bun_international_mins_used_allowance_percentage { get; set; }
            public string roam_bun_sms_allowance { get; set; }
            public string roam_bun_sms_used_allowance_percentage { get; set; }
            public string roam_bun_most_international_mins_used_country { get; set; }
            public string roam_bun_most_international_mins_used_country_percentage { get; set; }
            public string roam_bun_vectone_to_vectone_minutes_allowance { get; set; }
            public string roam_bun_vectone_to_vectone_minutes_used_allowance_percentage { get; set; }
            public string latest_bundle_id_being_purchased { get; set; }
            public string Has_active_SIMONLY_bundle { get; set; }
            public string current_SIMONLY_bundle_renewal_date { get; set; }
            public string latest_bundle_purchase_date { get; set; }
            public string latest_bundle_payment_mode { get; set; }
            public string latest_bundle_payment_method { get; set; }
            public string latest_bundle_payment_reference_number { get; set; }
            public string current_addon_Data_enddate { get; set; }
            public string current_addon_Data_startdate { get; set; }
            public string current_addon_international_enddate { get; set; }
            public string current_addon_international_startdate { get; set; }
            public string current_addon_national_enddate { get; set; }
            public string current_addon_national_startdate { get; set; }
            public DateTime? current_AIO_enddate { get; set; }
            public DateTime? current_AIO_startdate { get; set; }
            public DateTime? current_Data_enddate { get; set; }
            public DateTime? current_Data_startdate { get; set; }
            public DateTime? current_international_enddate { get; set; }
            public DateTime? current_international_startdate { get; set; }
            public DateTime? current_national_enddate { get; set; }
            public DateTime? current_national_startdate { get; set; }
            public DateTime? current_SIMONLY_enddate { get; set; }
            public DateTime? current_SIMONLY_startdate { get; set; }
            public string current_data_roaming_addon_name { get; set; }
            public string current_data_roaming_addon_id { get; set; }
            public string current_data_roaming_addon_startdate { get; set; }
            public string current_data_roaming_addon_enddate { get; set; }
            public string data_roaming_addon_data_total_allowance { get; set; }
            public string data_roaming_addon_data_used_allowance_percentage { get; set; }
            public string number_of_addons_purchased { get; set; }
            public string number_of_active_addons { get; set; }
            public string latest_addon_being_purchased { get; set; }
            public string Has_active_national_addon { get; set; }
            public string Has_active_international_addon { get; set; }
            public string Has_active_data_roaming_addon { get; set; }
            public string Has_active_data_addon { get; set; }
            public string roaming_status { get; set; }
            public string auto_renewal { get; set; }
            public string auto_renewal_disable_by { get; set; }
            public string auto_renewal_disable_date { get; set; }
            public string auto_renewal_enable_by { get; set; }
            public string auto_renewal_enable_date { get; set; }
            public int? nat_call_package_id { get; set; }
            public int? int_call_package_id { get; set; }
            public int? data_package_id { get; set; }
            public int? sms_package_id { get; set; }
            public int? v2v_pack_id { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            //public int? hubspot_contact_id	{ get; set; }
        }

        public static List<UrcrmusergetdetailsOutput> GetActiveCustomers(UrcrmusergetdetailsInput objUrcrmusergetdetailsInput)
        {
            List<UrcrmusergetdetailsOutput> OutputList = new List<UrcrmusergetdetailsOutput>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ESPMCM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "customer_marketing_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @from_date = objUrcrmusergetdetailsInput.fromdate,
                                @to_date = objUrcrmusergetdetailsInput.todate
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsOutput
                        {
                            iccid = r.iccid,
                            firstupdate = r.firstupdate,
                            lastupdate = r.lastupdate,
                            mobileno = r.mobileno,
                            customer_type = r.customer_type,
                            freesimid = r.freesimid,
                            auto_topup = r.auto_topup == null ? 0 : r.auto_topup,
                            subscriberid = r.subscriberid,
                            firstname = r.firstname,
                            lastname = r.lastname,
                            email = r.email,
                            houseno = r.houseno,
                            address1 = r.address1,
                            address2 = r.address2,
                            postcode = r.postcode,
                            //Temp commentd
                            //joined = r.joined,
                            current_national_bundle_id = r.current_national_bundle_id == null ? 0 : r.current_national_bundle_id,
                            current_national_bundle_name = r.current_national_bundle_name,
                            current_national_startdate = r.current_national_startdate,
                            current_national_enddate = r.current_national_enddate,
                            current_international_bundle_id = r.current_international_bundle_id == null ? 0 : r.current_international_bundle_id,
                            current_international_bundle_name = r.current_international_bundle_name,
                            current_international_startdate = r.current_international_startdate,
                            current_international_enddate = r.current_international_enddate,
                            current_Data_bundle_id = r.current_Data_bundle_id == null ? 0 : r.current_Data_bundle_id,
                            current_Data_bundle_name = r.current_Data_bundle_name,
                            current_Data_startdate = r.current_Data_startdate,
                            current_Data_enddate = r.current_Data_enddate,
                            current_AIO_bundle_id = r.current_AIO_bundle_id == null ? 0 : r.current_Data_bundle_id,
                            current_AIO_bundle_name = r.current_AIO_bundle_name,
                            current_AIO_startdate = r.current_AIO_startdate,
                            current_AIO_enddate = r.current_AIO_enddate,
                            current_SIMONLY_bundle_id = r.current_SIMONLY_bundle_id == null ? 0 : r.current_SIMONLY_bundle_id,
                            current_SIMONLY_bundle_name = r.current_SIMONLY_bundle_name,
                            current_SIMONLY_startdate = r.current_SIMONLY_startdate,
                            current_SIMONLY_enddate = r.current_SIMONLY_enddate,
                            contact_id = r.contact_id == null ? 0 : r.contact_id,
                            auto_renewal = r.auto_renewal,
                            Smart_Rates_user = r.Smart_Rates_user,
                            latest_topup_method_used = r.latest_topup_method_used,
                            latest_topup_amount = r.latest_topup_amount == null ? 0D : r.latest_topup_amount,
                            Country_code = r.Country_code,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        UrcrmusergetdetailsOutput outputobj = new UrcrmusergetdetailsOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmusergetdetailsOutput outputobj = new UrcrmusergetdetailsOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }
        static bool IsValidEmail(string email)
        {

            string pattern = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            if (Regex.IsMatch(email, pattern))
            {
                if (email.Contains("yahoo.clm") || email.Contains("yahoo.co.ok")
                    || email.Contains(".cim")
                    || email.Contains(".cim")
                    || email.Contains(".con")
                    || email.Contains(".con")
                    || email.Contains(".comn"))
                    return false;
                else
                {
                    try
                    {
                        if ((email.Contains("yahoo.co.ok")))
                            return false;
                        var addr = new System.Net.Mail.MailAddress(email);
                        return addr.Address == email;
                    }
                    catch
                    {
                        return false;
                    }
                }

            }
            else
            {
                return false;
            }

        }

        public static List<UrcrmusergetdetailsOutput2> GetSyncCustomers1(SyncCustomersInput objSyncCustomersInput)
        {
            List<UrcrmusergetdetailsOutput2> OutputList = new List<UrcrmusergetdetailsOutput2>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["hubspotapi"].ConnectionString))
                {
                    conn.Open();
                    //var sp = "HUBSPOT_GET_ACTIVITY_DETAILS_test_dummy";  //testing
                    //var sp = "HUBSPOT_GET_ACTIVITY_DETAILS_1";
                    var sp = "HUBSPOT_GET_ACTIVITY_DETAILS";  //Live
                    // var sp = "HUBSPOT_GET_ACTIVITY_DETAILS_last";  //Live
                    // var sp = "hubs_last_push";  //Live
                    // var sp = "hubs_last_push_check";  //Live

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@contact_id = objSyncCustomersInput.contact_id,
                                //@mobileno = objSyncCustomersInput.mobileno,
                                //@processtype = objSyncCustomersInput.processtype
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsOutput2
                        {
                            auto_topup = r.auto_topup,
                            auto_topup_disable_by = r.auto_topup_disable_by,
                            auto_topup_disable_date = r.auto_topup_disable_date,
                            auto_topup_enable_by = r.auto_topup_enable_by,
                            auto_topup_enable_date = r.auto_topup_enable_date,
                            first_topup_amount = r.first_topup_amount == null ? 0.0D : r.first_topup_amount,
                            //Temp Commented
                            first_topup_method_used = r.first_topup_method_used,
                            first_time_topup_date = r.first_time_topup_date,
                            latest_auto_topup_attempt_date = r.latest_auto_topup_attempt_date,
                            latest_auto_topup_attempt_failure_reason = r.latest_auto_topup_attempt_failure_reason,
                            latest_auto_topup_attempt_status = r.latest_auto_topup_attempt_status,
                            latest_topup_amount = r.latest_topup_amount == null ? 0.0D : r.latest_topup_amount,
                            latest_topup_type = r.latest_topup_type,
                            iccid = r.iccid,
                            sim_inserted = r.sim_inserted,
                            sim_type = r.sim_type,
                            freesimid = r.freesimid == null ? 0 : r.freesimid,
                            subscriberid = r.subscriberid == null ? 0 : r.subscriberid,
                            firstname = r.firstname,
                            lastname = r.lastname,
                            email = r.email,
                            houseno = r.houseno,
                            address1 = r.address1,
                            address2 = r.address2,
                            city = r.city,
                            state = r.state,
                            mobilephone = r.mobilephone,
                            number_of_bundles_purchased = r.number_of_bundles_purchased == null ? 0 : r.number_of_bundles_purchased,
                            number_of_active_bundles = r.number_of_active_bundles == null ? 0 : r.number_of_active_bundles,
                            first_bundle_name = r.first_bundle_name,
                            first_time_purchased_bundle_category = r.first_time_purchased_bundle_category,
                            first_bundle_purchase_amount = r.first_bundle_purchase_amount == null ? 0 : r.first_bundle_purchase_amount,
                            first_bundle_purchase_date = r.first_bundle_purchase_date,
                            first_bundle_purchase_method = r.first_bundle_purchase_method,
                            first_time_bundle_purchase_payment_method_used = r.first_time_bundle_purchase_payment_method_used,
                            first_bundle_status = r.first_bundle_status,
                            latest_bundle_renewal_attempt_date = r.latest_bundle_renewal_attempt_date,
                            latest_bundle_renewal_attempt_failure_reason = r.latest_bundle_renewal_attempt_failure_reason,
                            latest_bundle_renewal_attempt_status = r.latest_bundle_renewal_attempt_status,
                            latest_bundle_name_being_purchased = r.latest_bundle_name_being_purchased,
                            National_addon_Bundle_national_mins_total_allowance = r.National_addon_Bundle_national_mins_total_allowance == null ? 0.0D : r.National_addon_Bundle_national_mins_total_allowance,
                            National_Bundle_data_allowance = r.National_Bundle_data_allowance == null ? 0.0D : r.National_Bundle_data_allowance,
                            National_Bundle_data_used_allowance_percentage = r.National_Bundle_data_used_allowance_percentage == null ? 0.0D : r.National_Bundle_data_used_allowance_percentage,
                            National_Bundle_international_mins_total_allowance = r.National_Bundle_international_mins_total_allowance == null ? 0.0D : r.National_Bundle_international_mins_total_allowance,
                            National_Bundle_international_mins_used_allowance_percentage = r.National_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.National_Bundle_international_mins_used_allowance_percentage,
                            National_Bundle_national_mins_total_allowance = r.National_Bundle_national_mins_total_allowance == null ? 0.0D : r.National_Bundle_national_mins_total_allowance,
                            National_Bundle_national_mins_used_allowance_percentage = r.National_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.National_Bundle_national_mins_used_allowance_percentage,
                            National_Bundle_sms_allowance = r.National_Bundle_sms_allowance == null ? 0.0D : r.National_Bundle_sms_allowance,
                            National_Bundle_sms_used_allowance_percentage = r.National_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.National_Bundle_sms_used_allowance_percentage,
                            National_Bundle_vectone_to_vectone_minutes_allowance = r.National_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.National_Bundle_vectone_to_vectone_minutes_allowance,
                            AIO_Bundle_data_allowance = r.AIO_Bundle_data_allowance == null ? 0.0D : r.AIO_Bundle_data_allowance,
                            AIO_Bundle_data_used_allowance_percentage = r.AIO_Bundle_data_used_allowance_percentage == null ? 0.0D : r.AIO_Bundle_data_used_allowance_percentage,
                            AIO_Bundle_international_mins_total_allowance = r.AIO_Bundle_international_mins_total_allowance == null ? 0.0D : r.AIO_Bundle_international_mins_total_allowance,
                            AIO_Bundle_international_mins_used_allowance_percentage = r.AIO_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.AIO_Bundle_international_mins_used_allowance_percentage,
                            AIO_Bundle_national_mins_total_allowance = r.AIO_Bundle_national_mins_total_allowance == null ? 0.0D : r.AIO_Bundle_national_mins_total_allowance,
                            AIO_Bundle_national_mins_used_allowance_percentage = r.AIO_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.AIO_Bundle_national_mins_used_allowance_percentage,
                            AIO_Bundle_sms_allowance = r.AIO_Bundle_sms_allowance == null ? 0.0D : r.AIO_Bundle_sms_allowance,
                            AIO_Bundle_sms_used_allowance_percentage = r.AIO_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.AIO_Bundle_sms_used_allowance_percentage,
                            AIO_Bundle_vectone_to_vectone_minutes_allowance = r.AIO_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.AIO_Bundle_vectone_to_vectone_minutes_allowance,
                            AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.AIO_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            current_addon_Data_bundle_id = r.current_addon_Data_bundle_id == null ? 0 : r.current_addon_Data_bundle_id,
                            current_addon_Data_bundle_name = r.current_addon_Data_bundle_name,
                            current_addon_international_bundle_id = r.current_addon_international_bundle_id == null ? 0 : r.current_addon_international_bundle_id,
                            current_addon_international_bundle_name = r.current_addon_international_bundle_name,
                            current_addon_national_bundle_id = r.current_addon_national_bundle_id == null ? 0 : r.current_addon_national_bundle_id,
                            current_addon_national_bundle_name = r.current_addon_national_bundle_name,
                            current_AIO_bundle_id = r.current_AIO_bundle_id == null ? 0 : r.current_AIO_bundle_id,
                            current_AIO_bundle_name = r.current_AIO_bundle_name,
                            current_AIO_bundle_renewal_date = r.current_AIO_bundle_renewal_date,
                            current_Data_bundle_id = r.current_Data_bundle_id == null ? 0 : r.current_Data_bundle_id,
                            current_Data_bundle_name = r.current_Data_bundle_name,
                            current_data_bundle_renewal_date = r.current_data_bundle_renewal_date,
                            current_international_bundle_id = r.current_international_bundle_id == null ? 0 : r.current_international_bundle_id,
                            current_international_bundle_name = r.current_international_bundle_name,
                            current_International_bundle_renewal_date = r.current_International_bundle_renewal_date,
                            current_national_bundle_id = r.current_national_bundle_id == null ? 0 : r.current_national_bundle_id,
                            current_national_bundle_name = r.current_national_bundle_name,
                            current_national_bundle_renewal_date = r.current_national_bundle_renewal_date,
                            current_SIMONLY_bundle_id = r.current_SIMONLY_bundle_id == null ? 0 : r.current_SIMONLY_bundle_id,
                            current_SIMONLY_bundle_name = r.current_SIMONLY_bundle_name,
                            Data_addon_Bundle_data_allowance = r.Data_addon_Bundle_data_allowance == null ? 0.0D : r.Data_addon_Bundle_data_allowance,
                            Data_addon_Bundle_data_used_allowance_percentage = r.Data_addon_Bundle_data_used_allowance_percentage == null ? 0.0D : r.Data_addon_Bundle_data_used_allowance_percentage,
                            //Data_addon_Bundle_international_mins_used_allowance_percentage = r.Data_addon_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.Data_addon_Bundle_international_mins_used_allowance_percentage  ,	
                            Data_Bundle_data_allowance = r.Data_Bundle_data_allowance == null ? 0.0D : r.Data_Bundle_data_allowance,
                            Data_Bundle_data_used_allowance_percentage = r.Data_Bundle_data_used_allowance_percentage == null ? 0.0D : r.Data_Bundle_data_used_allowance_percentage,
                            Data_Bundle_international_mins_total_allowance = r.Data_Bundle_international_mins_total_allowance == null ? 0.0D : r.Data_Bundle_international_mins_total_allowance,
                            Data_Bundle_international_mins_used_allowance_percentage = r.Data_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.Data_Bundle_international_mins_used_allowance_percentage,
                            Data_Bundle_national_mins_total_allowance = r.Data_Bundle_national_mins_total_allowance == null ? 0.0D : r.Data_Bundle_national_mins_total_allowance,
                            Data_Bundle_national_mins_used_allowance_percentage = r.Data_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.Data_Bundle_national_mins_used_allowance_percentage,
                            Data_Bundle_sms_allowance = r.Data_Bundle_sms_allowance == null ? 0.0D : r.Data_Bundle_sms_allowance,
                            Data_Bundle_sms_used_allowance_percentage = r.Data_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.Data_Bundle_sms_used_allowance_percentage,
                            Data_Bundle_vectone_to_vectone_minutes_allowance = r.Data_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.Data_Bundle_vectone_to_vectone_minutes_allowance,
                            Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.Data_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            International_addon_Bundle_international_mins_total_allowance = r.International_addon_Bundle_international_mins_total_allowance == null ? 0.0D : r.International_addon_Bundle_international_mins_total_allowance,
                            International_addon_Bundle_international_mins_used_allowance_percentage = r.International_addon_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.International_addon_Bundle_international_mins_used_allowance_percentage,
                            International_Bundle_data_allowance = r.International_Bundle_data_allowance == null ? 0.0D : r.International_Bundle_data_allowance,
                            International_Bundle_data_used_allowance_percentage = r.International_Bundle_data_used_allowance_percentage == null ? 0.0D : r.International_Bundle_data_used_allowance_percentage,
                            International_Bundle_international_mins_total_allowance = r.International_Bundle_international_mins_total_allowance == null ? 0.0D : r.International_Bundle_international_mins_total_allowance,
                            International_Bundle_international_mins_used_allowance_percentage = r.International_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.International_Bundle_international_mins_used_allowance_percentage,
                            International_Bundle_national_mins_total_allowance = r.International_Bundle_national_mins_total_allowance == null ? 0.0D : r.International_Bundle_national_mins_total_allowance,
                            International_Bundle_national_mins_used_allowance_percentage = r.International_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.International_Bundle_national_mins_used_allowance_percentage,
                            International_Bundle_sms_allowance = r.International_Bundle_sms_allowance == null ? 0.0D : r.International_Bundle_sms_allowance,
                            International_Bundle_sms_used_allowance_percentage = r.International_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.International_Bundle_sms_used_allowance_percentage,
                            International_Bundle_vectone_to_vectone_minutes_allowance = r.International_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.International_Bundle_vectone_to_vectone_minutes_allowance,
                            International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.International_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            latest_sim_with_bundle_name_being_purchased = r.latest_sim_with_bundle_name_being_purchased,
                            latest_sim_only_with_bundle_name_being_purchased = r.latest_sim_only_with_bundle_name_being_purchased,
                            National_addon_Bundle_national_mins_used_allowance_percentage = r.National_addon_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.National_addon_Bundle_national_mins_used_allowance_percentage,
                            National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.National_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            revenue_from_bundle = r.revenue_from_bundle == null ? 0.0D : r.revenue_from_bundle,
                            simonly_Bundle_data_allowance = r.simonly_Bundle_data_allowance == null ? 0.0D : r.simonly_Bundle_data_allowance,
                            simonly_Bundle_data_used_allowance_percentage = r.simonly_Bundle_data_used_allowance_percentage == null ? 0.0D : r.simonly_Bundle_data_used_allowance_percentage,
                            simonly_Bundle_international_mins_total_allowance = r.simonly_Bundle_international_mins_total_allowance == null ? 0.0D : r.simonly_Bundle_international_mins_total_allowance,
                            simonly_Bundle_international_mins_used_allowance_percentage = r.simonly_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.simonly_Bundle_international_mins_used_allowance_percentage,
                            simonly_Bundle_national_mins_total_allowance = r.simonly_Bundle_national_mins_total_allowance == null ? 0.0D : r.simonly_Bundle_national_mins_total_allowance,
                            simonly_Bundle_national_mins_used_allowance_percentage = r.simonly_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.simonly_Bundle_national_mins_used_allowance_percentage,
                            simonly_Bundle_sms_allowance = r.simonly_Bundle_sms_allowance == null ? 0.0D : r.simonly_Bundle_sms_allowance,
                            simonly_Bundle_sms_used_allowance_percentage = r.simonly_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.simonly_Bundle_sms_used_allowance_percentage,
                            simonly_Bundle_vectone_to_vectone_minutes_allowance = r.simonly_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.simonly_Bundle_vectone_to_vectone_minutes_allowance,
                            simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.simonly_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            current_national_bundle_most_international_mins_used_country = r.current_national_bundle_most_international_mins_used_country == null ? 0.0D : r.current_national_bundle_most_international_mins_used_country,
                            current_international_bundle_most_international_mins_used_country = r.current_international_bundle_most_international_mins_used_country == null ? 0.0D : r.current_international_bundle_most_international_mins_used_country,
                            current_AIO_bundle_most_international_mins_used_country = r.current_AIO_bundle_most_international_mins_used_country == null ? 0.0D : r.current_AIO_bundle_most_international_mins_used_country,
                            current_SIMONly_bundle_most_international_mins_used_country = r.current_SIMONly_bundle_most_international_mins_used_country == null ? 0.0D : r.current_SIMONly_bundle_most_international_mins_used_country,
                            National_Bundle_most_international_mins_used_country_percentage = r.National_Bundle_most_international_mins_used_country_percentage == null ? 0.0D : r.National_Bundle_most_international_mins_used_country_percentage,
                            International_Bundle_most_international_mins_used_country_percentage = r.International_Bundle_most_international_mins_used_country_percentage == null ? 0.0D : r.International_Bundle_most_international_mins_used_country_percentage,
                            AIO_Bundle_most_international_mins_used_country_percentage = r.AIO_Bundle_most_international_mins_used_country_percentage == null ? 0.0D : r.AIO_Bundle_most_international_mins_used_country_percentage,
                            Data_Bundle_most_international_mins_used_country = r.Data_Bundle_most_international_mins_used_country == null ? 0.0D : r.Data_Bundle_most_international_mins_used_country,
                            Data_Bundle_most_international_mins_used_country_percentage = r.Data_Bundle_most_international_mins_used_country_percentage == null ? 0.0D : r.Data_Bundle_most_international_mins_used_country_percentage,
                            current_SIMONly_bundle_most_international_mins_used_country_per = r.current_SIMONly_bundle_most_international_mins_used_country_per == null ? 0.0D : r.current_SIMONly_bundle_most_international_mins_used_country_per,
                            previously_subscribed_for_country_specific_bundles_ = r.previously_subscribed_for_country_specific_bundles_,
                            most_subscribed_country_specific_bundle_name = r.most_subscribed_country_specific_bundle_name,
                            Has_active_national_bundle = r.Has_active_national_bundle,
                            Has_active_AIO_bundle = r.Has_active_AIO_bundle,
                            Has_active_International_bundle = r.Has_active_International_bundle,
                            Has_active_data_bundle = r.Has_active_data_bundle,
                            Has_active_roaming_bundle = r.Has_active_roaming_bundle,
                            latest_bundle_id_being_purchased = r.latest_bundle_id_being_purchased == null ? 0 : r.latest_bundle_id_being_purchased,
                            Has_active_SIMONLY_bundle = r.Has_active_SIMONLY_bundle,
                            current_SIMONLY_bundle_renewal_date = r.current_SIMONLY_bundle_renewal_date,
                            latest_bundle_purchase_date = r.latest_bundle_purchase_date,
                            latest_bundle_payment_mode = r.latest_bundle_payment_mode,
                            latest_bundle_payment_method = r.latest_bundle_payment_method,
                            latest_bundle_payment_reference_number = r.latest_bundle_payment_reference_number,
                            current_addon_Data_enddate = r.current_addon_Data_enddate,
                            current_addon_Data_startdate = r.current_addon_Data_startdate,
                            current_addon_international_enddate = r.current_addon_international_enddate,
                            current_addon_international_startdate = r.current_addon_international_startdate,
                            current_addon_national_enddate = r.current_addon_national_enddate,
                            current_addon_national_startdate = r.current_addon_national_startdate,
                            current_AIO_enddate = r.current_AIO_enddate,
                            current_AIO_startdate = r.current_AIO_startdate,
                            current_Data_enddate = r.current_Data_enddate,
                            current_Data_startdate = r.current_Data_startdate,
                            current_international_enddate = r.current_international_enddate,
                            current_international_startdate = r.current_international_startdate,
                            current_national_enddate = r.current_national_enddate,
                            current_national_startdate = r.current_national_startdate,
                            current_SIMONLY_enddate = r.current_SIMONLY_enddate,
                            current_SIMONLY_startdate = r.current_SIMONLY_startdate,
                            current_data_roaming_addon_name = r.current_data_roaming_addon_name,
                            current_data_roaming_addon_id = r.current_data_roaming_addon_id == null ? 0 : r.current_data_roaming_addon_id,
                            current_data_roaming_addon_startdate = r.current_data_roaming_addon_startdate,
                            current_data_roaming_addon_enddate = r.current_data_roaming_addon_enddate,
                            data_roaming_addon_data_total_allowance = r.data_roaming_addon_data_total_allowance == null ? 0.0D : r.data_roaming_addon_data_total_allowance,
                            data_roaming_addon_data_used_allowance_percentage = r.data_roaming_addon_data_used_allowance_percentage == null ? 0.0D : r.data_roaming_addon_data_used_allowance_percentage,
                            number_of_addons_purchased = r.number_of_addons_purchased == null ? 0 : r.number_of_addons_purchased,
                            number_of_active_addons = r.number_of_active_addons == null ? 0 : r.number_of_active_addons,
                            latest_addon_being_purchased = r.latest_addon_being_purchased,
                            Has_active_national_addon = r.Has_active_national_addon,
                            Has_active_international_addon = r.Has_active_international_addon,
                            Has_active_data_roaming_addon = r.Has_active_data_roaming_addon,
                            Has_active_data_addon = r.Has_active_data_addon,
                            roaming_status = r.roaming_status,
                            auto_renewal = r.auto_renewal,
                            auto_renewal_disable_by = r.auto_renewal_disable_by,
                            auto_renewal_disable_date = r.auto_renewal_disable_date,
                            auto_renewal_enable_by = r.auto_renewal_enable_by,
                            auto_renewal_enable_date = r.auto_renewal_enable_date,
                            alternate_email_id = r.alternate_email_id,
                            //arpu = r.arpu == null ? 0.0D : r.arpu  ,	
                            avg_customer_wait_time_between_purchases = r.avg_customer_wait_time_between_purchases == null ? 0 : r.avg_customer_wait_time_between_purchases,
                            country = r.country,
                            country_code = r.country_code,
                            current_balance = r.current_balance == null ? 0.0D : r.current_balance,
                            current_roaming_bundle_name = r.current_roaming_bundle_name,
                            current_roaming_bundle_renewal_date = r.current_roaming_bundle_renewal_date,
                            current_roaming_bundle_startdate = r.current_roaming_bundle_startdate,
                            customer_type = r.customer_type,
                            date_of_birth = r.date_of_birth,
                            //Temp Commented
                            email_verification_status = r.email_verification_status,
                            first_referral_date = r.first_referral_date,
                            //Temp commented
                            first_time_topup_payment_method_used = r.first_time_topup_payment_method_used,
                            first_topup_status = r.first_topup_status,
                            free_credit = r.free_credit == null ? 0.0D : r.free_credit,
                            frequent_topup_method = r.frequent_topup_method,
                            has_llom = r.has_llom,
                            has_vectone_extra = r.has_vectone_extra,

                            //Temp commented
                            hs_analytics_source = r.hs_analytics_source,
                            hs_language = r.hs_language,
                            hs_lead_status = r.hs_lead_status,
                            hs_object_id = r.hs_object_id,
                            international_topup_country_name = r.international_topup_country_name,
                            international_topup_network_name = r.international_topup_network_name,
                            last_location_update_date = r.last_location_update_date,
                            last_referral_date = r.last_referral_date,
                            lastcalldate = r.lastcalldate,
                            lastgprsdate = r.lastgprsdate,
                            lastsmsdate = r.lastsmsdate,
                            latest_attempt_payment_reference_number = r.latest_attempt_payment_reference_number,
                            latest_buying_stage = r.latest_buying_stage,
                            latest_international_topup_amount = r.latest_international_topup_amount == null ? 0.0D : r.latest_international_topup_amount,
                            latest_international_topup_request_completion_status = r.latest_international_topup_request_completion_status,
                            latest_international_topup_request_failure_reason = r.latest_international_topup_request_failure_reason,
                            latest_payment_attempt_failure_reason = r.latest_payment_attempt_failure_reason,
                            Latest_Preloaded_SIM_Amount_Being_Purchased = r.Latest_Preloaded_SIM_Amount_Being_Purchased,
                            latest_product_being_purchased = r.latest_product_being_purchased,
                            latest_purchase_attempt_date = r.latest_purchase_attempt_date,
                            latest_purchase_attempt_payment_method = r.latest_purchase_attempt_payment_method,
                            //Temp commented
                            latest_purchase_attempt_payment_mode = r.latest_purchase_attempt_payment_mode,
                            latest_purchase_attempt_payment_status = r.latest_purchase_attempt_payment_status,
                            Latest_SIM_with_Topup_Amount_Being_Purchased = r.Latest_SIM_with_Topup_Amount_Being_Purchased == null ? 0 : r.Latest_SIM_with_Topup_Amount_Being_Purchased,//Need to check with DB Team	
                            latest_topup_amount_being_purchased = r.latest_topup_amount_being_purchased == null ? 0 : r.latest_topup_amount_being_purchased,
                            latest_topup_date = r.latest_topup_date,
                            //Temp Commented
                            latest_topup_method_used1 = r.latest_topup_method_used1,
                            latest_topup_payment_method_used = r.latest_topup_payment_method_used,
                            latest_topup_payment_reference_number = r.latest_topup_payment_reference_number,
                            lifecyclestage = r.lifecyclestage,
                            no_of_times_top_up_20_and_above = r.no_of_times_top_up_20_and_above == null ? 0 : r.no_of_times_top_up_20_and_above,
                            number_of_international_topups_made = r.number_of_international_topups_made == null ? 0 : r.number_of_international_topups_made,
                            number_of_referrals_made = r.number_of_referrals_made == null ? 0 : r.number_of_referrals_made,
                            number_of_topups_made = r.number_of_topups_made == null ? 0 : r.number_of_topups_made,
                            phone = r.phone,
                            portin_current_network_name = r.portin_current_network_name,

                            portin_request_completion_status = r.portin_request_completion_status,
                            portin_request = r.portin_request,
                            portin_status = r.portin_status,

                            portin_request_failure_reason = r.portin_request_failure_reason,
                            portout_network_name = r.portout_network_name,
                            reason_for_portout = r.reason_for_portout,
                            referee_subscriber_id = r.referee_subscriber_id == null ? 0 : r.referee_subscriber_id,
                            referred_someone = r.referred_someone,
                            referred_someone_first_time = r.referred_someone_first_time,
                            registered_to_myaccount = r.registered_to_myaccount,
                            revenue_from_topup = r.revenue_from_topup == null ? 0.0D : r.revenue_from_topup,
                            roaming_Bundle_data_allowance = r.roaming_Bundle_data_allowance == null ? 0.0D : r.roaming_Bundle_data_allowance,
                            roaming_Bundle_data_used_allowance_percentage = r.roaming_Bundle_data_used_allowance_percentage == null ? 0.0D : r.roaming_Bundle_data_used_allowance_percentage,
                            roaming_Bundle_international_mins_total_allowance = r.roaming_Bundle_international_mins_total_allowance == null ? 0.0D : r.roaming_Bundle_international_mins_total_allowance,
                            roaming_Bundle_international_mins_used_allowance_percentage = r.roaming_Bundle_international_mins_used_allowance_percentage == null ? 0.0D : r.roaming_Bundle_international_mins_used_allowance_percentage,
                            roaming_Bundle_most_international_mins_used_country = r.roaming_Bundle_most_international_mins_used_country == null ? 0.0D : r.roaming_Bundle_most_international_mins_used_country,
                            roaming_Bundle_most_international_mins_used_country_percentage = r.roaming_Bundle_most_international_mins_used_country_percentage == null ? 0.0D : r.roaming_Bundle_most_international_mins_used_country_percentage,
                            roaming_Bundle_national_mins_total_allowance = r.roaming_Bundle_national_mins_total_allowance == null ? 0.0D : r.roaming_Bundle_national_mins_total_allowance,
                            roaming_Bundle_national_mins_used_allowance_percentage = r.roaming_Bundle_national_mins_used_allowance_percentage == null ? 0.0D : r.roaming_Bundle_national_mins_used_allowance_percentage,
                            roaming_Bundle_sms_allowance = r.roaming_Bundle_sms_allowance == null ? 0.0D : r.roaming_Bundle_sms_allowance,
                            roaming_Bundle_sms_used_allowance_percentage = r.roaming_Bundle_sms_used_allowance_percentage == null ? 0.0D : r.roaming_Bundle_sms_used_allowance_percentage,
                            roaming_Bundle_vectone_to_vectone_minutes_allowance = r.roaming_Bundle_vectone_to_vectone_minutes_allowance == null ? 0.0D : r.roaming_Bundle_vectone_to_vectone_minutes_allowance,
                            roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage = r.roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage == null ? 0.0D : r.roaming_Bundle_vectone_to_vectone_minutes_used_allowance_percentage,
                            sim_country = r.sim_country,
                            sim_inserted_date = r.sim_inserted_date,
                            sim_order_status = r.sim_order_status,
                            SIM_ordered_by = r.SIM_ordered_by,
                            Smart_Rates_user = r.Smart_Rates_user,
                            total_amount_of_international_topup_made = r.total_amount_of_international_topup_made == null ? 0.0D : r.total_amount_of_international_topup_made,
                            total_purchase_amount_of_the_customer = r.total_purchase_amount_of_the_customer == null ? 0.0D : r.total_purchase_amount_of_the_customer,
                            sim_dispatch_date = r.sim_dispatch_date,
                            zip = r.zip,
                            brand_name = r.brand_name,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            credit_allowance_used = r.credit_allowance_used,
                            nextbillingdate = r.nextbillingdate,
                            landlinenumber = r.landlinenumber,
                            existing_customer_my_vectone_charge_back = r.existing_customer_my_vectone_charge_back,
                            requested_for_sim_swap = r.requested_for_sim_swap,
                            sim_swap_status = r.sim_swap_status,
                            simonly_excessusage = r.simonly_excessusage,
                            last_topup_vou_method = r.last_topup_vou_method,
                            lat_product_vou_failed_reason = r.lat_product_vou_failed_reason,
                            latest_product_voucher_attempt_status = r.latest_product_voucher_attempt_status,
                            types_of_bundle = r.types_of_bundle,

                           // sim_only_rollover_data_allowance = r.sim_only_rollover_data_allowance,
                            credit_card_number = r.cc_no,
                            credit_card_expiry_date = r.cc_no_expirydate,
                            credit_card_type = r.cc_card_type

                            // plan_upgrade = r.plan_upgrade,
                            //plan_downgrade = r. plan_downgrade,
                            //plan_change_date = r.plan_change_date,
                            //change_plan_requested_name = r.change_plan_requested_name 
                        }));
                    }
                    else
                    {

                        UrcrmusergetdetailsOutput2 outputobj = new UrcrmusergetdetailsOutput2();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                SendMail(ex.Message.ToString(), "DB Mapping error", "Hubspot DB Error", "dotnetteam@vectone.com", "phpteam@vectone.com", "y.abdulrasul@vectone.com", "DBdevteam@vectone.com", "p.loganathan@vectone.com", "s.devendran@vectone.com", "m.manokaran@vectone.com");
                UrcrmusergetdetailsOutput2 outputobj = new UrcrmusergetdetailsOutput2();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }
        public static List<UrcrmusergetdetailsContactOutput> GetContactIDCheck(SyncCustomersInput objSyncCustomersInput)
        {
            List<UrcrmusergetdetailsContactOutput> OutputList = new List<UrcrmusergetdetailsContactOutput>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["hubspotupdateapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "hubspot_exe_contact_check";
                    // var sp = "hubs_last_after_push";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = objSyncCustomersInput.mobileno
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsContactOutput
                        {
                            ContactID = r.ContactID,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        UrcrmusergetdetailsContactOutput outputobj = new UrcrmusergetdetailsContactOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmusergetdetailsContactOutput outputobj = new UrcrmusergetdetailsContactOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }
        public static List<UrcrmusergetdetailsOutput1> GetSyncCustomersupdate(SyncCustomersInput objSyncCustomersInput)
        {
            List<UrcrmusergetdetailsOutput1> OutputList = new List<UrcrmusergetdetailsOutput1>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["hubspotupdateapi"].ConnectionString))
                {
                    conn.Open();

                    if (objSyncCustomersInput.iccid == null)
                        objSyncCustomersInput.iccid = "";

                    var sp = "hubspot_exe_update_contact_info";
                    // var sp = "hubs_last_after_push";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @hubspot_contact_id = objSyncCustomersInput.contact_id,
                                @mobileno = objSyncCustomersInput.mobileno,
                                @iccid = objSyncCustomersInput.iccid
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Write("Hubspot_exe_update_contact_info success Result: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.iccid);

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsOutput1
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        Write("Hubspot_exe_update_contact_info No Result: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.iccid);
                        UrcrmusergetdetailsOutput1 outputobj = new UrcrmusergetdetailsOutput1();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }

                }
            }
            catch (Exception ex)
            {
                Write("Exception hubspot_exe_update_contact_info: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.iccid);
                UrcrmusergetdetailsOutput1 outputobj = new UrcrmusergetdetailsOutput1();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }

        public static List<UrcrmusergetdetailsOutput1> GetSyncCustomersdeletecontact(SyncCustomersInput objSyncCustomersInput)
        {
            List<UrcrmusergetdetailsOutput1> OutputList = new List<UrcrmusergetdetailsOutput1>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["hubspotupdateapi"].ConnectionString))
                {
                    conn.Open();

                    if (objSyncCustomersInput.iccid == null)
                        objSyncCustomersInput.iccid = "";

                    var sp = "hubspot_exe_delete_contact_info";
                    // var sp = "hubs_last_after_push";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = objSyncCustomersInput.mobileno
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Write("GetSyncCustomersdeletecontact success Result: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.contact_id);

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsOutput1
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        Write("Hubspot_exe_update_contact_info No Result: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.iccid);
                        UrcrmusergetdetailsOutput1 outputobj = new UrcrmusergetdetailsOutput1();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }

                }
            }
            catch (Exception ex)
            {
                Write("Exception hubspot_exe_update_contact_info: " + objSyncCustomersInput.contact_id.ToString() + "-" + objSyncCustomersInput.mobileno + " -" + objSyncCustomersInput.iccid);
                UrcrmusergetdetailsOutput1 outputobj = new UrcrmusergetdetailsOutput1();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }
        public static List<UrcrmusergetdetailsOutput> GetSyncCustomers(SyncCustomersInput objSyncCustomersInput)
        {
            List<UrcrmusergetdetailsOutput> OutputList = new List<UrcrmusergetdetailsOutput>();
            List<Contact> objList = new List<Contact>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ESPMCM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "customer_marketing_info_sync_records";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @contact_id = objSyncCustomersInput.contact_id,
                                @mobileno = objSyncCustomersInput.mobileno,
                                @processtype = objSyncCustomersInput.processtype
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {

                        OutputList.AddRange(result.Select(r => new UrcrmusergetdetailsOutput
                        {
                            iccid = r.iccid,
                            firstupdate = r.firstupdate,
                            lastupdate = r.lastupdate,
                            mobileno = r.mobileno,
                            customer_type = r.customer_type,
                            freesimid = r.freesimid,
                            auto_topup = r.auto_topup,
                            subscriberid = r.subscriberid,
                            firstname = r.firstname,
                            lastname = r.lastname,
                            email = r.email,
                            houseno = r.houseno,
                            address1 = r.address1,
                            address2 = r.address2,
                            postcode = r.postcode,
                            //Temp commented
                            //joined = r.joined,
                            current_national_bundle_id = r.current_national_bundle_id == null ? 0 : r.current_national_bundle_id,
                            current_national_bundle_name = r.current_national_bundle_name,
                            current_national_startdate = r.current_national_startdate,
                            current_national_enddate = r.current_national_enddate,
                            current_international_bundle_id = r.current_international_bundle_id == null ? 0 : r.current_international_bundle_id,
                            current_international_bundle_name = r.current_international_bundle_name,
                            current_international_startdate = r.current_international_startdate,
                            current_international_enddate = r.current_international_enddate,
                            current_Data_bundle_id = r.current_Data_bundle_id == null ? 0 : r.current_Data_bundle_id,
                            current_Data_bundle_name = r.current_Data_bundle_name,
                            current_Data_startdate = r.current_Data_startdate,
                            current_Data_enddate = r.current_Data_enddate,
                            current_AIO_bundle_id = r.current_AIO_bundle_id == null ? 0 : r.current_Data_bundle_id,
                            current_AIO_bundle_name = r.current_AIO_bundle_name,
                            current_AIO_startdate = r.current_AIO_startdate,
                            current_AIO_enddate = r.current_AIO_enddate,
                            current_SIMONLY_bundle_id = r.current_SIMONLY_bundle_id == null ? 0 : r.current_SIMONLY_bundle_id,
                            current_SIMONLY_bundle_name = r.current_SIMONLY_bundle_name,
                            current_SIMONLY_startdate = r.current_SIMONLY_startdate,
                            current_SIMONLY_enddate = r.current_SIMONLY_enddate,
                            contact_id = r.contact_id == null ? 0 : r.contact_id,
                            auto_renewal = r.auto_renewal,
                            Smart_Rates_user = r.Smart_Rates_user,
                            latest_topup_method_used = r.latest_topup_method_used,
                            latest_topup_amount = r.latest_topup_amount == null ? 0D : r.latest_topup_amount,
                            Country_code = r.Country_code,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        UrcrmusergetdetailsOutput outputobj = new UrcrmusergetdetailsOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmusergetdetailsOutput outputobj = new UrcrmusergetdetailsOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;

        }

        public static List<UrcrmusergetdetailsOutput> GetBundleHistory()
        {
            return null;
        }

    }
    public class Identity
    {
        public string value { get; set; }
        public string type { get; set; }
        public object timestamp { get; set; }
        public bool isPrimary { get; set; }
    }

    public class IdentityProfile
    {
        public int vid { get; set; }
        public List<Identity> identity { get; set; }
        public List<object> linkedVid { get; set; }
        public bool isContact { get; set; }
        public long savedAtTimestamp { get; set; }
    }

    public class Error
    {
        public string message { get; set; }
        public string @in { get; set; }
    }

    public class UpdateContactObject
    {
        public string message { get; set; }
        public IdentityProfile identityProfile { get; set; }
        public List<Error> errors { get; set; }
        public string status { get; set; }
        public string correlationId { get; set; }
        public string category { get; set; }
        public string error { get; set; }
        public string requestId { get; set; }
    }
    public class Contact
    {
        public string property { get; set; }
        public string value { get; set; }
    }

    public class Properties
    {
        public List<Contact> properties { get; set; }
    }
    public class Column
    {
        public string name { get; set; }
        public string label { get; set; }
        public int id { get; set; }
        public string type { get; set; }
    }

    public class AddColumnDetails
    {
        public List<Column> columns { get; set; }
    }

}


