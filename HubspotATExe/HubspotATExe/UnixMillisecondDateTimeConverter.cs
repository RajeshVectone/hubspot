﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime;

namespace HubspotATExe
{
    class UnixMillisecondDateTimeConverter
    {
        public static double convertDateTimeToEpoch(DateTime date)
        {
            //var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            //return Convert.ToInt64((date - epoch).TotalSeconds);
            //date = System.DateTime.SpecifyKind(date, DateTimeKind.Utc);
            //var utcValue = ((DateTimeOffset)date).ToUnixTimeMilliseconds();
            //return utcValue;

            //return date.Subtract(    
            //    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            //    ).TotalSeconds;

            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long milliseconds;
            milliseconds = (long)(date - UnixEpoch).TotalMilliseconds;
            return milliseconds;


        }
      
        public static string convertDateTimeToEpochvalue(DateTime? date)
        {
            //var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            //return Convert.ToInt64((date - epoch).TotalSeconds);
            //date = System.DateTime.SpecifyKind(date, DateTimeKind.Utc);
            //var utcValue = ((DateTimeOffset)date).ToUnixTimeMilliseconds();
            //return utcValue;

            //return date.Subtract(    
            //    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            //    ).TotalSeconds;

            DateTime dt = Convert.ToDateTime(date).Date; 
            double date1 = UnixMillisecondDateTimeConverter.convertDateTimeToEpoch(dt);
            return date1.ToString();
        }
    }
}
